# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 10:07:45 2020

@author: wtheo
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

pos_offset=pd.read_csv("tek0008.csv", sep=',', header=19)
neg_offset=pd.read_csv("tek0009.csv", sep=',', header=19)

x1=pos_offset.iloc[:,0]+50
x2=pos_offset.iloc[:,0]+50

y1=100*pos_offset.iloc[:,1]
y2=pos_offset.iloc[:,2]

plt.figure(figsize=(12.8,9.6))
plt.plot(x1, y1, linewidth=1, label="Optical signal")
plt.plot(x2, y2, linewidth=1, label="Electrical signal")
plt.legend()
plt.grid()
plt.xlabel("Time /s")
plt.ylabel("Voltage /V")
plt.show()

xstart1=6466
xstart2=53405

xstart=xstart1
xend=xstart2
x1=pos_offset.iloc[xstart:xend,0]+50
x2=pos_offset.iloc[xstart:xend,0]+50

y1=100*pos_offset.iloc[xstart:xend,1]
y2=pos_offset.iloc[xstart:xend,2]

f=lambda x, a, b, c: a/(x+b)+c
popt, pcov = curve_fit(f, x1, y1, p0=[3, -4, 1.7])
print(popt)

f2=lambda x, a, b, c, d: a*np.exp(b*x+c)+d
popt2, pcov = curve_fit(f2, x1, y1, p0=[0.6, -0.33, 3, 1.7])
print(popt2)


plt.figure(figsize=(12.8,9.6))
plt.plot(x1, y1, linewidth=1, label="Optical signal")
plt.plot(x1, f(x1, *popt), label="f(x)=3.03/(x-4.01)+1.71")
plt.plot(x1, f2(x1, *popt2), label="f(x)=0.78exp(-2.0x+1.47)+1.81")
plt.plot(x2, y2, linewidth=1, label="Electrical signal")
plt.legend()
plt.grid()
plt.xlabel("Time /s")
plt.ylabel("Voltage /V")
plt.show()

xstart=xstart2
xend=100000
x1=pos_offset.iloc[xstart:xend,0]+50
x2=pos_offset.iloc[xstart:xend,0]+50

y1=100*pos_offset.iloc[xstart:xend,1]
y2=pos_offset.iloc[xstart:xend,2]

f=lambda x, a, b, c: -a/(x+b)+c
popt, pcov = curve_fit(f, x1, y1, p0=[3, -4, 1])
print(popt)

f2=lambda x, a, b, c, d: -a*np.exp(b*x+c)+d
popt2, pcov = curve_fit(f2, x1, y1, p0=[0.6, -0.33, 3, 1])
print(popt2)


plt.figure(figsize=(12.8,9.6))
plt.plot(x1, y1, linewidth=1, label="Optical signal")
plt.plot(x1, f(x1, *popt), label="f(x)=1.05-2.12/(x-50.78)")
plt.plot(x1, f2(x1, *popt2), label="f(x)=0.99-9.85exp(-0.18x+6.72)")
plt.plot(x2, y2, linewidth=1, label="Electrical signal")
plt.legend()
plt.grid()
plt.xlabel("Time /s")
plt.ylabel("Voltage /V")
plt.show()