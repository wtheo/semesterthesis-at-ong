# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


#photodiode characterisation
g=10**0 #gain factor
g_m=3   #gain mutliplier
r=0.55  #response factor in V/mW

def convert(voltage):
    return voltage/g/g_m/r


#%%
#tek0002
#0.1 Hz
#5 Vpp
#-1.4 V offset

hz01 = pd.read_csv("tek0002.csv", sep=',', header=19 )
dummyx=np.arange(10000)/100

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("100 mHz modulation frequency, 5 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(hz01[["CH1"]]), color="C0", label="Optical signal")
ax2.plot(dummyx, hz01[["CH2"]], color="C0", label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
ax1.legend()
ax2.legend()
plt.savefig("100mHz_5V_V-t.png")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 1 mHz")
plt.scatter(hz01[["CH2"]], convert(hz01[["CH1"]]), marker='x', label="All data points")
plt.scatter(hz01.iloc[:500,2], convert(hz01.iloc[:500,1]), marker='x', label="First branch")
plt.scatter(hz01.iloc[500:1000,2], convert(hz01.iloc[500:1000,1]), marker='x', label="Second branch")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("100mHz_5V_I-V.png")
plt.show()

#%%
#tek0003
#1 Hz
#5 Vpp
#-1.4 V offset

hz02 = pd.read_csv("tek0003.csv", sep=',', header=19 )
dummyx=np.arange(10000)/1000

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("1 Hz modulation frequency")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(hz02[["CH1"]]), color="C1", label="Optical signal")
ax2.plot(dummyx, hz02[["CH2"]], color="C1", label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
ax1.legend()
ax2.legend()
plt.savefig("1Hz_5V_V-t.png")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 1 Hz")
plt.scatter(hz02[["CH2"]], convert(hz02[["CH1"]]), marker='x', label="All data points")
plt.scatter(hz02.iloc[:500,2], convert(hz02.iloc[:500,1]), marker='x', label="First branch")
plt.scatter(hz02.iloc[500:1000,2], convert(hz02.iloc[500:1000,1]), marker='x', label="Second branch")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("1Hz_5V_I-V.png")
plt.show()

#%%
#tek0004
#10 Hz
#5 Vpp
#-1.4 V offset

hz03 = pd.read_csv("tek0004.csv", sep=',', header=19 )
dummyx=np.arange(10000)/10000

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("10 Hz modulation frequency")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(hz03[["CH1"]]), color="C2", label="Optical signal")
ax2.plot(dummyx, hz03[["CH2"]], color="C2", label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power (mW)")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
ax1.legend()
ax2.legend()
plt.savefig("10Hz_5V_V-t.png")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 10 Hz")
plt.scatter(hz03[["CH2"]], convert(hz03[["CH1"]]), marker='x', label="All data points")
plt.scatter(hz03.iloc[200:700,2], convert(hz03.iloc[200:700,1]), marker='x', label="First branch")
plt.scatter(hz03.iloc[700:1200,2], convert(hz03.iloc[700:1200,1]), marker='x', label="Second branch")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("10Hz_5V_I-V.png")
plt.show()

#%%
#tek0005
#100 Hz
#5 Vpp
#-1.4 V offset

hz04 = pd.read_csv("tek0005.csv", sep=',', header=19 )
dummyx=np.arange(1000)/10000

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("100 Hz modulation frequency")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(hz04.iloc[0:1000,1]), color="C3", label="Optical signal")
ax2.plot(dummyx, hz04.iloc[0:1000,2], color="C3", label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
ax1.legend()
ax2.legend()
plt.savefig("100Hz_5V_V-t.png")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 100 Hz")
plt.scatter(hz04[["CH2"]], convert(hz04[["CH1"]]), marker='x', label="All data points")
plt.scatter(hz04.iloc[250:750,2], convert(hz04.iloc[250:750,1]), marker='x', label="First branch")
plt.scatter(hz04.iloc[750:1250,2], convert(hz04.iloc[750:1250,1]), marker='x', label="Second branch")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("100Hz_5V_I-V.png")
plt.show()

#%%
#tek0007
#1 kHz
#5 Vpp
#-1.4 V offset

hz05 = pd.read_csv("tek0007.csv", sep=',', header=19 )
dummyx=np.arange(500)/1000

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("1 kHz modulation frequency")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx*0.04, convert(hz05.iloc[0:500,1]), color="C4", label="Optical signal")
ax2.plot(dummyx*0.04, hz05.iloc[0:500,2], color="C4", label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power (mW)")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
ax1.legend()
ax2.legend()
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.scatter(hz05[["CH2"]], convert(hz05[["CH1"]]), marker='x')
plt.show()

#%%
#tek0008
#10 kHz
#5 Vpp
#-1.4 V offset

hz06 = pd.read_csv("tek0008.csv", sep=',', header=19 )
dummyx=np.arange(100)/1000

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("10 kHz modulation frequency")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx*0.04, hz06.iloc[0:100,1], color="C5", label="Optical signal")
ax2.plot(dummyx*0.04, hz06.iloc[0:100,2], color="C5", label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Voltage /V")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
ax1.legend()
ax2.legend()
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.scatter(hz06[["CH2"]], hz06[["CH1"]], marker='x')
plt.show()

#%%
#tek0010
#100 kHz
#5 Vpp
#-1.4 V offset

hz07 = pd.read_csv("tek0010.csv", sep=',', header=19 )
dummyx=np.arange(10000)*2*10**(-8)

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("100 kHz modulation frequency")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, hz07.iloc[:,1], color="C6", label="Optical signal")
ax2.plot(dummyx, hz07.iloc[:,2], color="C6", label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Voltage /V")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
ax1.legend()
ax2.legend()
plt.show()