# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 09:43:54 2020

@author: wtheo
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


#photodiode characterisation
g=10**0 #gain factor
g_m=3   #gain mutliplier
r=0.55  #response factor in V/mW

def convert(voltage):
    return voltage/g/g_m/r


def findmax(data):
    max=data.iloc[0]
    index=0
    for i in range (data.size):
        if data.iloc[i]>max:
            max=data.iloc[i]
            index=i
    return max, index


def sinfunc(x, a, b, c, d):
    return a*np.sin(2*np.pi/b*x-c)+d


def sinfit(xdata, ydata):
    a0=findmax(ydata)[0]-findmax(-ydata)[0]
    b0=2*(xdata.iloc[findmax(ydata)[1]]-xdata.iloc[findmax(-ydata)[1]])
    c0=xdata.iloc[findmax(ydata)[1]]-b0/4
    d0=np.sum(ydata)/ydata.size
    popt, pcov=curve_fit(sinfunc, xdata, ydata, p0=[a0, b0, c0, d0])
    return popt, pcov

def linfunc(x, a, b):
    return a*x+b


def linfit(xdata, ydata):
    a0=(ydata.iloc[-1]-ydata.iloc[0])/(xdata.iloc[-1]-xdata.iloc[0])
    b0=ydata.iloc[-1]-a0*xdata.iloc[-1]
    popt, pcov=curve_fit(linfunc, xdata, ydata, p0=[a0, b0])
    return popt, pcov

#%%
#black current
#tek0017
#no electric signal
#no optical signal

data6=pd.read_csv("tek0017.csv", sep=',', header=19)
d={"col1": np.arange(10000)/10000*0.4}
dummyx=pd.DataFrame(data=d)

avgval=sum(data6["CH1"])/data6.shape[0]
avg = pd.DataFrame(np.zeros(10000), columns=["col1"])
avg["col1"] = avgval

plt.figure(figsize=(12.8,9.6))
plt.title("Dark current")
plt.plot(dummyx[["col1"]], data6[["CH1"]], linewidth=1)
plt.plot(dummyx[["col1"]], avg[["col1"]])
plt.ylabel("Voltage /V")
plt.xlabel("Time /s")
plt.ylim((-0.002, -0.0025))
plt.grid()
plt.savefig("dark_current.png")
plt.savefig("dark_current.eps")
plt.show()

#%%
#calculates power ratio for default input power of 30mW in dB
def pow_spec(value, power=30):
    return 10*np.log((value-convert(avgval))/power)/np.log(10)

#%%
#tek0013
#100 mHz
#5 V pp
#280 mV offset
#30 mW @1550nm

data0=pd.read_csv("tek0013.csv", sep=',', header=19)
dummyx=np.arange(10000)/500

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("100 mHz modulation frequency, 5.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data0[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data0[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("100mHz_5Vpp_V-t.png")
plt.savefig("100mHz_5Vpp_V-t.eps")
plt.show()

popt0, pcov0=sinfit(data0.iloc[525:3025,2], convert(data0.iloc[525:3025,1]))
print("f_1(x)=", popt0[0], "*sin(", 2*np.pi/popt0[1], "x+", -popt0[2], ")+", popt0[3])

popt00, pcov00=sinfit(data0.iloc[3025:5525,2], convert(data0.iloc[3025:5525,1]))
print("f_2(x)=", popt00[0], "*sin(", 2*np.pi/popt00[1], "x+", -popt00[2], ")+", popt00[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 100 mHz")
plt.scatter(data0[["CH2"]], convert(data0[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data0.iloc[525:3025,2], convert(data0.iloc[525:3025,1]), marker='x', s=10, label="first branch down")
plt.scatter(data0.iloc[3025:5525,2], convert(data0.iloc[3025:5525,1]), marker='x', s=10, label="second branch up")
plt.plot(data0.iloc[525:3025,2], sinfunc(data0.iloc[525:3025,2], *popt0), color="C4", linewidth=2, label="f(x)=-0.024sin(1.0x+2.6)+0.024")
plt.plot(data0.iloc[3025:5525,2], sinfunc(data0.iloc[3025:5525,2], *popt00), color="C5", linewidth=2, label="f(x)=0.027sin(1.1x-0.16)+0.027")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("100mHz_5Vpp_I-V.png")
plt.savefig("100mHz_5Vpp_I-V.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 100 mHz modulation")
plt.scatter(data0[["CH2"]], pow_spec(convert(data0[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data0.iloc[525:3025,2], pow_spec(convert(data0.iloc[525:3025,1])), marker='x', s=10, label="first branch down")
plt.scatter(data0.iloc[3025:5525,2], pow_spec(convert(data0.iloc[3025:5525,1])), marker='x', s=10, label="second branch up")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("100mHz_5Vpp_I-V_dB.png")
plt.savefig("100mHz_5Vpp_I-V_dB.eps")
plt.show()

ex_ratio0a=findmax(pow_spec(convert(data0.iloc[525:3025,1])))[0]+findmax(-pow_spec(convert(data0.iloc[525:3025,1])))[0]
ex_ratio0b=findmax(pow_spec(convert(data0.iloc[3025:5525,1])))[0]+findmax(-pow_spec(convert(data0.iloc[3025:5525,1])))[0]
ex_ratio0=np.array([(ex_ratio0a+ex_ratio0b)/2, np.abs(ex_ratio0a-ex_ratio0b)/2])

#%%
#tek0014
#100 mHz
#1 V pp
#280 mV offset
#30 mW @1550nm

data1=pd.read_csv("tek0014.csv", sep=',', header=19)
dummyx=np.arange(10000)/500

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("100 mHz modulation frequency, 1.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data1[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data1[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("100mHz_1Vpp_V-t.png")
plt.savefig("100mHz_1Vpp_V-t.eps")
plt.show()

popt10, pcov10=linfit(data1.iloc[1875:4375,2], convert(data1.iloc[1875:4375,1]))
print("f_1(x)=", popt10[0], "*x+", popt10[1])

popt11, pcov11=linfit(data1.iloc[4375:6875,2], convert(data1.iloc[4375:6875,1]))
print("f_2(x)=", popt11[0], "*x+", popt11[1])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 100 mHz")
plt.scatter(data1[["CH2"]], convert(data1[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data1.iloc[1875:4375,2], convert(data1.iloc[1875:4375,1]), marker='x', s=10, label="first branch down")
plt.scatter(data1.iloc[4375:6875,2], convert(data1.iloc[4375:6875,1]), marker='x', s=10, label="second branch up")
plt.plot(data1.iloc[1875:4375,2], linfunc(data1.iloc[1875:4375,2], *popt10), color="C4", linewidth=2, label="f(x)=0.032x+0.016")
plt.plot(data1.iloc[4375:6875,2], linfunc(data1.iloc[4375:6875,2], *popt11), color="C5", linewidth=2, label="f(x)=0.033x+0.018")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("100mHz_1Vpp_I-V.png")
plt.savefig("100mHz_1Vpp_I-V.eps")
plt.show()

#%%
#tek0011
#1 Hz
#5 V pp
#280 mV offset
#30 mW @1550nm

data2=pd.read_csv("tek0011.csv", sep=',', header=19)
dummyx=np.arange(10000)/500

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("1 Hz modulation frequency, 5.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data2[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data2[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("1Hz_5Vpp_V-t.png")
plt.savefig("1Hz_5Vpp_V-t.eps")
plt.show()

popt20, pcov20=sinfit(data2.iloc[200:450,2], convert(data2.iloc[200:450,1]))
print("f_1(x)=", popt20[0], "*sin(", 2*np.pi/popt20[1], "x+", -popt20[2], ")+", popt20[3])

popt21, pcov21=sinfit(data2.iloc[450:700,2], convert(data2.iloc[450:700,1]))
print("f_2(x)=", popt21[0], "*sin(", 2*np.pi/popt21[1], "x+", -popt21[2], ")+", popt21[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 1 Hz")
plt.scatter(data2[["CH2"]], convert(data2[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data2.iloc[200:450,2], convert(data2.iloc[200:450,1]), marker='x', s=10, label="first branch up")
plt.scatter(data2.iloc[450:700,2], convert(data2.iloc[450:700,1]), marker='x', s=10, label="second branch down")
plt.plot(data2.iloc[200:450,2], sinfunc(data2.iloc[200:450,2], *popt20), color="C4", linewidth=2, label="f(x)=0.029sin(1.2x-0.17)+0.029")
plt.plot(data2.iloc[450:700,2], sinfunc(data2.iloc[450:700,2], *popt21), color="C5", linewidth=2, label="f(x)=0.028sin(1.3x-0.27)+0.028")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("1Hz_5Vpp_I-V.png")
plt.savefig("1Hz_5Vpp_I-V.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 1 Hz modulation")
plt.scatter(data2[["CH2"]], pow_spec(convert(data2[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data2.iloc[200:450,2], pow_spec(convert(data2.iloc[200:450,1])), marker='x', s=10, label="first branch up")
plt.scatter(data2.iloc[450:700,2], pow_spec(convert(data2.iloc[450:700,1])), marker='x', s=10, label="second branch down")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("1Hz_5Vpp_I-V_dB.png")
plt.savefig("1Hz_5Vpp_I-V_dB.eps")
plt.show()

ex_ratio2a=findmax(pow_spec(convert(data2.iloc[200:450,1])))[0]+findmax(-pow_spec(convert(data2.iloc[200:450,1])))[0]
ex_ratio2b=findmax(pow_spec(convert(data2.iloc[450:700,1])))[0]+findmax(-pow_spec(convert(data2.iloc[450:700,1])))[0]
ex_ratio2=np.array([(ex_ratio2a+ex_ratio2b)/2, np.abs(ex_ratio2a-ex_ratio2b)/2])

#%%
#tek0012
#1 Hz
#1 V pp
#280 mV offset
#30 mW @1550nm

data3=pd.read_csv("tek0012.csv", sep=',', header=19)
dummyx=np.arange(10000)/500

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("1 Hz modulation frequency, 1.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data3[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data3[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("1Hz_1Vpp_V-t.png")
plt.savefig("1Hz_1Vpp_V-t.eps")
plt.show()

popt30, pcov30=linfit(data3.iloc[200:450,2], convert(data3.iloc[200:450,1]))
print("f_1(x)=", popt30[0], "*x+", popt30[1])

popt31, pcov31=linfit(data3.iloc[450:700,2], convert(data3.iloc[450:700,1]))
print("f_2(x)=", popt31[0], "*x+", popt31[1])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 1 Hz")
plt.scatter(data3[["CH2"]], convert(data3[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data3.iloc[200:450,2], convert(data3.iloc[200:450,1]), marker='x', s=10, label="first branch up")
plt.scatter(data3.iloc[450:700,2], convert(data3.iloc[450:700,1]), marker='x', s=10, label="second branch down")
plt.plot(data3.iloc[200:450,2], linfunc(data3.iloc[200:450,2], *popt30), color="C4", linewidth=2, label="f(x)=0.032x+0.018")
plt.plot(data3.iloc[450:700,2], linfunc(data3.iloc[450:700,2], *popt31), color="C5", linewidth=2, label="f(x)=0.032x+0.017")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("1Hz_1Vpp_I-V.png")
plt.savefig("1Hz_1Vpp_I-V.eps")
plt.show()

#%%
#tek0015
#10 Hz
#5 V pp
#280 mV offset
#30 mW @1550nm

data4=pd.read_csv("tek0015.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000*0.4

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("10 Hz modulation frequency, 5.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data4[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data4[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("10Hz_5Vpp_V-t.png")
plt.savefig("10Hz_5Vpp_V-t.eps")
plt.show()

popt40, pcov40=sinfit(data4.iloc[675:1925,2], convert(data4.iloc[675:1925,1]))
print("f_1(x)=", popt40[0], "*sin(", 2*np.pi/popt40[1], "x+", -popt40[2], ")+", popt40[3])

popt41, pcov41=sinfit(data4.iloc[1925:3175,2], convert(data4.iloc[1925:3175,1]))
print("f_2(x)=", popt41[0], "*sin(", 2*np.pi/popt41[1], "x+", -popt41[2], ")+", popt41[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 10 Hz")
plt.scatter(data4[["CH2"]], convert(data4[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data4.iloc[675:1925,2], convert(data4.iloc[675:1925,1]), marker='x', s=10, label="first branch up")
plt.scatter(data4.iloc[1925:3175,2], convert(data4.iloc[1925:3175,1]), marker='x', s=10, label="second branch down")
plt.plot(data4.iloc[675:1925,2], sinfunc(data4.iloc[675:1925,2], *popt40), color="C4", linewidth=2, label="f(x)=0.026sin(1.2x-0.53)+0.025")
plt.plot(data4.iloc[1925:3175,2], sinfunc(data4.iloc[1925:3175,2], *popt41), color="C5", linewidth=2, label="f(x)=0.025sin(1.1x-0.099)+0.026")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("10Hz_5Vpp_I-V.png")
plt.savefig("10Hz_5Vpp_I-V.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 10 Hz modulation")
plt.scatter(data4[["CH2"]], pow_spec(convert(data4[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data4.iloc[675:1925,2], pow_spec(convert(data4.iloc[675:1925,1])), marker='x', s=10, label="first branch up")
plt.scatter(data4.iloc[1925:3175,2], pow_spec(convert(data4.iloc[1925:3175,1])), marker='x', s=10, label="second branch down")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("10Hz_5Vpp_I-V_dB.png")
plt.savefig("10Hz_5Vpp_I-V_dB.eps")
plt.show()

ex_ratio4a=findmax(pow_spec(convert(data4.iloc[675:1925,1])))[0]+findmax(-pow_spec(convert(data4.iloc[675:1925,1])))[0]
ex_ratio4b=findmax(pow_spec(convert(data4.iloc[1925:3175,1])))[0]+findmax(-pow_spec(convert(data4.iloc[1925:3175,1])))[0]
ex_ratio4=np.array([(ex_ratio4a+ex_ratio4b)/2, np.abs(ex_ratio4a-ex_ratio4b)/2])

#%%
#tek0016
#10 Hz
#1 V pp
#280 mV offset
#30 mW @1550nm

data5=pd.read_csv("tek0016.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000*0.4

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("10 Hz modulation frequency, 1.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data5[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data5[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("10Hz_1Vpp_V-t.png")
plt.savefig("10Hz_1Vpp_V-t.eps")
plt.show()

popt50, pcov50=linfit(data5.iloc[250:1500,2], convert(data5.iloc[250:1500,1]))
print("f_1(x)=", popt50[0], "*x+", popt50[1])

popt51, pcov51=linfit(data5.iloc[1500:2750,2], convert(data5.iloc[1500:2750,1]))
print("f_2(x)=", popt51[0], "*x+", popt51[1])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 10 Hz")
plt.scatter(data5[["CH2"]], convert(data5[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data5.iloc[250:1500,2], convert(data5.iloc[250:1500,1]), marker='x', s=10, label="first branch down")
plt.scatter(data5.iloc[1500:2750,2], convert(data5.iloc[1500:2750,1]), marker='x', s=10, label="second branch up")
plt.plot(data5.iloc[250:1500,2], linfunc(data5.iloc[250:1500,2], *popt50), color="C4", linewidth=2, label="f(x)=0.032x+0.018")
plt.plot(data5.iloc[1500:2750,2], linfunc(data5.iloc[1500:2750,2], *popt51), color="C5", linewidth=2, label="f(x)=0.032x+0.017")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("10Hz_1Vpp_I-V.png")
plt.savefig("10Hz_1Vpp_I-V.eps")
plt.show()

#%%
#tek0026
#100 Hz
#5 V pp
#880 mV offset
#30 mW @1550nm

data6=pd.read_csv("tek0026.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000*0.04

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("100 Hz modulation frequency, 5.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data6[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data6[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("100Hz_5Vpp_V-t.png")
plt.savefig("100Hz_5Vpp_V-t.eps")
plt.show()

popt60, pcov60=sinfit(data6.iloc[730:1980,2], convert(data6.iloc[730:1980,1]))
print("f_1(x)=", popt60[0], "*sin(", 2*np.pi/popt60[1], "x+", -popt60[2], ")+", popt60[3])

popt61, pcov61=sinfit(data6.iloc[1980:3230,2], convert(data6.iloc[1980:3230,1]))
print("f_2(x)=", popt61[0], "*sin(", 2*np.pi/popt61[1], "x+", -popt61[2], ")+", popt61[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 100 Hz")
plt.scatter(data6[["CH2"]], convert(data6[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data6.iloc[730:1980,2], convert(data6.iloc[730:1980,1]), marker='x', s=10, label="first branch down")
plt.scatter(data6.iloc[1980:3230,2], convert(data6.iloc[1980:3230,1]), marker='x', s=10, label="second branch up")
plt.plot(data6.iloc[730:1980,2], sinfunc(data6.iloc[730:1980,2], *popt60), color="C4", linewidth=2, label="f(x)=0.030sin(1.1x-1.0)+0.030")
plt.plot(data6.iloc[1980:3230,2], sinfunc(data6.iloc[1980:3230,2], *popt61), color="C5", linewidth=2, label="f(x)=0.030sin(1.1x-1.0)+0.030")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("100Hz_5Vpp_I-V.png")
plt.savefig("100Hz_5Vpp_I-V.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 100 Hz modulation")
plt.scatter(data6[["CH2"]], pow_spec(convert(data6[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data6.iloc[730:1980,2], pow_spec(convert(data6.iloc[730:1980,1])), marker='x', s=10, label="first branch down")
plt.scatter(data6.iloc[1980:3230,2], pow_spec(convert(data6.iloc[1980:3230,1])), marker='x', s=10, label="second branch up")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("100Hz_5Vpp_I-V_dB.png")
plt.savefig("100Hz_5Vpp_I-V_dB.eps")
plt.show()

ex_ratio6a=findmax(pow_spec(convert(data6.iloc[730:1980,1])))[0]+findmax(-pow_spec(convert(data6.iloc[730:1980,1])))[0]
ex_ratio6b=findmax(pow_spec(convert(data6.iloc[1980:3230,1])))[0]+findmax(-pow_spec(convert(data6.iloc[1980:3230,1])))[0]
ex_ratio6=np.array([(ex_ratio6a+ex_ratio6b)/2, np.abs(ex_ratio6a-ex_ratio6b)/2])

#%%
#tek0027
#100 Hz
#1 V pp
#880 mV offset
#30 mW @1550nm

data7=pd.read_csv("tek0027.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000*0.1

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("100 Hz modulation frequency, 1.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data7[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data7[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("100Hz_1Vpp_V-t.png")
plt.savefig("100Hz_1Vpp_V-t.eps")
plt.show()

popt70, pcov70=linfit(data7.iloc[240:740,2], convert(data7.iloc[240:740,1]))
print("f_1(x)=", popt70[0], "*x+", popt70[1])

popt71, pcov71=linfit(data7.iloc[740:1240,2], convert(data7.iloc[740:1240,1]))
print("f_2(x)=", popt71[0], "*x+", popt71[1])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 100 Hz")
plt.scatter(data7[["CH2"]], convert(data7[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data7.iloc[240:740,2], convert(data7.iloc[240:740,1]), marker='x', s=10, label="first branch down")
plt.scatter(data7.iloc[740:1240,2], convert(data7.iloc[740:1240,1]), marker='x', s=10, label="second branch up")
plt.plot(data7.iloc[240:740,2], linfunc(data7.iloc[240:740,2], *popt70), color="C4", linewidth=2, label="f(x)=0.033x+0.0028")
plt.plot(data7.iloc[740:1240,2], linfunc(data7.iloc[740:1240,2], *popt71), color="C5", linewidth=2, label="f(x)=0.033x+0.0028")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("100Hz_1Vpp_I-V.png")
plt.savefig("100Hz_1Vpp_I-V.eps")
plt.show()

#%%
#tek0029
#1 kHz
#5 V pp
#880 mV offset
#30 mW @1550nm

data8=pd.read_csv("tek0029.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000*0.01

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("1 kHz modulation frequency, 5.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data8[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data8[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("1kHz_5Vpp_V-t.png")
plt.savefig("1kHz_5Vpp_V-t.eps")
plt.show()

popt80, pcov80=sinfit(data8.iloc[380:880,2], convert(data8.iloc[380:880,1]))
print("f_1(x)=", popt80[0], "*sin(", 2*np.pi/popt80[1], "x+", -popt80[2], ")+", popt80[3])

popt81, pcov81=sinfit(data8.iloc[880:1380,2], convert(data8.iloc[880:1380,1]))
print("f_2(x)=", popt81[0], "*sin(", 2*np.pi/popt81[1], "x+", -popt81[2], ")+", popt81[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 1 kHz")
plt.scatter(data8[["CH2"]], convert(data8[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data8.iloc[380:880,2], convert(data8.iloc[380:880,1]), marker='x', s=10, label="first branch up")
plt.scatter(data8.iloc[880:1380,2], convert(data8.iloc[880:1380,1]), marker='x', s=10, label="second branch down")
plt.plot(data8.iloc[380:880,2], sinfunc(data8.iloc[380:880,2], *popt80), color="C4", linewidth=2, label="f(x)=0.030sin(1.1x-0.89)+0.029")
plt.plot(data8.iloc[880:1380,2], sinfunc(data8.iloc[880:1380,2], *popt81), color="C5", linewidth=2, label="f(x)=0.029sin(1.1x-0.82)+0.029")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("1kHz_5Vpp_I-V.png")
plt.savefig("1kHz_5Vpp_I-V.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 1 kHz modulation")
plt.scatter(data8[["CH2"]], pow_spec(convert(data8[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data8.iloc[380:880,2], pow_spec(convert(data8.iloc[380:880,1])), marker='x', s=10, label="first branch up")
plt.scatter(data8.iloc[880:1380,2], pow_spec(convert(data8.iloc[880:1380,1])), marker='x', s=10, label="second branch down")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("1kHz_5Vpp_I-V_dB.png")
plt.savefig("1kHz_5Vpp_I-V_dB.eps")
plt.show()

ex_ratio8a=findmax(pow_spec(convert(data8.iloc[380:880,1])))[0]+findmax(-pow_spec(convert(data8.iloc[380:880,1])))[0]
ex_ratio8b=findmax(pow_spec(convert(data8.iloc[880:1380,1])))[0]+findmax(-pow_spec(convert(data8.iloc[880:1380,1])))[0]
ex_ratio8=np.array([(ex_ratio8a+ex_ratio8b)/2, np.abs(ex_ratio8a-ex_ratio8b)/2])

#%%
#tek0028
#1 kHz
#1 V pp
#880 mV offset
#30 mW @1550nm

data9=pd.read_csv("tek0028.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000*0.01

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("1 kHz modulation frequency, 1.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data9[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data9[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("1kHz_1Vpp_V-t.png")
plt.savefig("1kHz_1Vpp_V-t.eps")
plt.show()

popt90, pcov90=linfit(data9.iloc[350:850,2], convert(data9.iloc[350:850,1]))
print("f_1(x)=", popt90[0], "*x+", popt90[1])

popt91, pcov91=linfit(data9.iloc[850:1350,2], convert(data9.iloc[850:1350,1]))
print("f_2(x)=", popt91[0], "*x+", popt91[1])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 1 kHz")
plt.scatter(data9[["CH2"]], convert(data9[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data9.iloc[350:850,2], convert(data9.iloc[350:850,1]), marker='x', s=10, label="first branch up")
plt.scatter(data9.iloc[850:1350,2], convert(data9.iloc[850:1350,1]), marker='x', s=10, label="second branch down")
plt.plot(data9.iloc[350:850,2], linfunc(data9.iloc[350:850,2], *popt90), color="C4", linewidth=2, label="f(x)=0.032x+0.0047")
plt.plot(data9.iloc[850:1350,2], linfunc(data9.iloc[850:1350,2], *popt91), color="C5", linewidth=2, label="f(x)=0.032x+0.0050")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("1kHz_1Vpp_I-V.png")
plt.savefig("1kHz_1Vpp_I-V.eps")
plt.show()

#%%
#tek0030
#10 kHz
#5 V pp
#880 mV offset
#30 mW @1550nm

data10=pd.read_csv("tek0030.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000*0.002

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("10 kHz modulation frequency, 5.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data10[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data10[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.plot(dummyx[70:320], convert(data10.iloc[70:320,1]), color="orange", label="first branch up")
ax1.plot(dummyx[320:570], convert(data10.iloc[320:570,1]), color="green", label="second branch down")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
ax1.set_xlim((0,0.00025))
ax2.set_xlim((0,0.00025))
ax1.legend()
ax2.legend()
plt.savefig("10kHz_5Vpp_V-t.png")
plt.savefig("10kHz_5Vpp_V-t.eps")
plt.show()

popt100, pcov100=sinfit(data10.iloc[100:300,2], convert(data10.iloc[100:300,1]))
print("f_1(x)=", popt100[0], "*sin(", 2*np.pi/popt100[1], "x+", -popt100[2], ")+", popt100[3])

popt101, pcov101=sinfit(data10.iloc[320:570,2], convert(data10.iloc[320:570,1]))
print("f_2(x)=", popt101[0], "*sin(", 2*np.pi/popt101[1], "x+", -popt101[2], ")+", popt101[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 10 kHz")
plt.scatter(data10[["CH2"]], convert(data10[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data10.iloc[70:320,2], convert(data10.iloc[70:320,1]), marker='x', s=10, label="first branch up")
plt.scatter(data10.iloc[320:570,2], convert(data10.iloc[320:570,1]), marker='x', s=10, label="second branch down")
plt.plot(data10.iloc[70:320,2], sinfunc(data10.iloc[70:320,2], *popt100), color="C4", linewidth=2, label="f(x)=0.030sin(1.1x-1.1)+0.029")
plt.plot(data10.iloc[320:570,2], sinfunc(data10.iloc[320:570,2], *popt101), color="C5", linewidth=2, label="f(x)=0.029sin(1.1x-0.55)+0.030")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("10kHz_5Vpp_I-V.png")
plt.savefig("10kHz_5Vpp_I-V.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 10 kHz modulation")
plt.scatter(data10[["CH2"]], pow_spec(convert(data10[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data10.iloc[70:320,2], pow_spec(convert(data10.iloc[70:320,1])), marker='x', s=10, label="first branch up")
plt.scatter(data10.iloc[320:570,2], pow_spec(convert(data10.iloc[320:570,1])), marker='x', s=10, label="second branch down")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("10kHz_5Vpp_I-V_dB.png")
plt.savefig("10kHz_5Vpp_I-V_dB.eps")
plt.show()

ex_ratio10a=findmax(pow_spec(convert(data10.iloc[70:320,1])))[0]+findmax(-pow_spec(convert(data10.iloc[70:320,1])))[0]
ex_ratio10b=findmax(pow_spec(convert(data10.iloc[320:570,1])))[0]+findmax(-pow_spec(convert(data10.iloc[320:570,1])))[0]
ex_ratio10=np.array([(ex_ratio10a+ex_ratio10b)/2, np.abs(ex_ratio10a-ex_ratio10b)/2])

#%%
#tek0028
#10 kHz
#1 V pp
#880 mV offset
#30 mW @1550nm

data11=pd.read_csv("tek0031.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000*0.002

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("10 kHz modulation frequency, 1.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data11[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data11[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("10kHz_1Vpp_V-t.png")
plt.savefig("10kHz_1Vpp_V-t.eps")
plt.show()

popt110, pcov110=linfit(data11.iloc[60:310,2], convert(data11.iloc[60:310,1]))
print("f_1(x)=", popt110[0], "*x+", popt110[1])

popt111, pcov111=linfit(data11.iloc[310:560,2], convert(data11.iloc[310:560,1]))
print("f_2(x)=", popt111[0], "*x+", popt111[1])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 10 kHz")
plt.scatter(data11[["CH2"]], convert(data11[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data11.iloc[60:310,2], convert(data11.iloc[60:310,1]), marker='x', s=10, label="first branch up")
plt.scatter(data11.iloc[310:560,2], convert(data11.iloc[310:560,1]), marker='x', s=10, label="second branch down")
plt.plot(data11.iloc[60:310,2], linfunc(data11.iloc[60:310,2], *popt110), color="C4", linewidth=2, label="f(x)=0.032x+0.0062")
plt.plot(data11.iloc[310:560,2], linfunc(data11.iloc[310:560,2], *popt111), color="C5", linewidth=2, label="f(x)=0.031x+0.0095")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("10kHz_1Vpp_I-V.png")
plt.savefig("10kHz_1Vpp_I-V.eps")
plt.show()

#%%

V_pi=np.array([(popt00[1]+popt0[1])/4, (popt20[1]+popt21[1])/4, (popt41[1]+popt40[1])/4, (popt60[1]+popt61[1])/4, (popt80[1]+popt81[1])/4, (popt100[1]+popt101[1])/4])
V_pistd=np.array([0.1, 0.05, 0.05, 0.04, 0.04, 0.07])
freq=np.array([0.1, 1, 10, 100, 1000, 10000])

params, sigmas=curve_fit(linfunc, np.log(freq[2:])/np.log(10), V_pi[2:], p0=[(V_pi[-1]-V_pi[2])/4, 2.72], sigma=V_pistd[2:], absolute_sigma=True)
print("f(x)=", params[0], "*log_10(x)+", params[1])
print(np.sqrt(np.diag(sigmas)))

dummy=np.arange(10**(-1), 10**4)

plt.figure(figsize=(12.8, 9.6))
plt.title("$V_\pi$ voltage for different modulation frequencies")
plt.errorbar(freq, V_pi, yerr=V_pistd, fmt='x', label="measurements")
plt.plot(dummy, linfunc(np.log(dummy)/np.log(10), *params), label=r"$f(x)=0.0090*\log_{10}(x)+2.7$")
plt.legend()
plt.grid()
plt.xlabel("Frequency /Hz")
plt.ylabel("$V_\pi$ Voltage /V")
plt.xscale("log")
plt.savefig("v-pi_800nm_different-freq.png")
plt.savefig("v-pi_800nm_different-freq.eps")
plt.show()

#%%


ex_ratios=np.stack((ex_ratio0, ex_ratio2, ex_ratio4, ex_ratio6, ex_ratio8, ex_ratio10))

print(ex_ratios)


