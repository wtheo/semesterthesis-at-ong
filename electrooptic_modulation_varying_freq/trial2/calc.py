# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 11:01:00 2020

@author: wtheo
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


#photodiode characterisation
g=10**1 #gain factor
g_m=3   #gain mutliplier
r=0.55  #response factor in V/mW

def convert(voltage):
    return voltage/g/g_m/r


#%%
#tek0002
#0.1 Hz
#700 mVpp / 400 mVpp
#-1.5 V offset


data1a = pd.read_csv("tek0002.csv", sep=',', header=19 )
data1b = pd.read_csv("tek0003.csv", sep=',', header=19)
dummyx = np.arange(10000)/100

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("100 mHz modulation frequency, 0.7 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data1a[["CH1"]]), color="C0", linestyle="None", marker='.', markersize=5, label="Optical signal")
ax2.plot(dummyx, data1a[["CH2"]], color="C0", linestyle="None", marker='.', markersize=5, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
ax1.legend()
ax2.legend()
plt.savefig("100mHz_700mV_V-t.png")
plt.show()

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("100 mHz modulation frequency, 0.4 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data1b[["CH1"]]), color="C0", linestyle="None", marker='.', markersize=5, label="Optical signal")
ax2.plot(dummyx, data1b[["CH2"]], color="C0", linestyle="None", marker='.', markersize=5, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Voltage /V")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
ax1.legend()
ax2.legend()
plt.savefig("100mHz_400mV_V-t.png")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 100 mHz with 700mV pp")
plt.scatter(data1a[["CH2"]], convert(data1a[["CH1"]]), marker='x', s=5, label="All data points")
plt.scatter(data1a.iloc[:500,2], convert(data1a.iloc[:500,1]), marker='x', s=5, label="First branch")
plt.scatter(data1a.iloc[500:1000,2], convert(data1a.iloc[500:1000,1]), marker='x', s=5, label="Second branch")
plt.grid()
plt.legend()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("100mHz_700mV_I-V.png")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 100 mHz with 400mV pp")
plt.scatter(data1b[["CH2"]], convert(data1b[["CH1"]]), marker='x', s=5, label="All data points")
plt.scatter(data1b.iloc[:500,2], convert(data1b.iloc[:500,1]), marker='x', s=5, label="First branch")
plt.scatter(data1b.iloc[500:1000,2], convert(data1b.iloc[500:1000,1]), marker='x', s=5, label="Second branch")
plt.grid()
plt.legend()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("100mHz_400mV_I-V.png")
plt.show()

#%%
#tek0008
#1 Hz
#1 V pp
#-1.8 V offset

data8=pd.read_csv("tek0008_hires.csv", sep=',', header=19)
dummyx=np.arange(10000)/500

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("1 Hz modulation frequency, 1.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data8[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data8[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("1Hz_V-t_hi-res.png")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 1 Hz")
plt.scatter(data8[["CH2"]], convert(data8[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data8.iloc[:500,2], convert(data8.iloc[:500,1]), marker='x', s=10, label="first branch")
plt.scatter(data8.iloc[500:1000,2], convert(data8.iloc[500:1000,1]), marker='x', s=10, label="second branch")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("1Hz_I-V_hi-res.png")
plt.show()

#%%
#tek0007
#1 kHz
#5 Vpp
#-1.4 V offset

data7=pd.read_csv("tek0007.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000/100*2

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("1 kHz modulation frequency, 5.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data7[["CH1"]]), color="C0",marker='.', markersize=5, label="Optical signal")
ax2.plot(dummyx, data7[["CH2"]], color="C0", marker='.', markersize=5, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
ax1.set_xlim((0,0.005))
ax2.set_xlim((0,0.005))
ax1.legend()
ax2.legend()
plt.savefig("1kHz_V-t_hi-res.png")
plt.show()

plt.figure(figsize=(12.8, 9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 1 kHz")
plt.scatter(data7[["CH2"]], convert(data7[["CH1"]]), marker='x', s=2, label="all data points")
plt.scatter(data7.iloc[:500,2], convert(data7.iloc[:500,1]), marker='x', s=10, label="first branch")
plt.scatter(data7.iloc[500:1000,2], convert(data7.iloc[500:1000,1]), marker='x', s=10, label="second branch")
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.grid()
plt.legend()
plt.savefig("1kHz_I-V_hi-res.png")
plt.show()

#%%
#tek0006
#10 kHz
#5 Vpp
#-1.4 V offset

data6=pd.read_csv("tek0006.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000/1000*4

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("10 kHz modulation frequency, 5.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data6[["CH1"]]), color="C0", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data6[["CH2"]], color="C0", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
ax1.set_xlim((0,0.0005))
ax2.set_xlim((0,0.0005))
ax1.legend()
ax2.legend()
plt.savefig("10kHz_V-t_hi-res")
plt.show()

plt.figure(figsize=(12.8, 9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 10 kHz")
plt.scatter(data6[["CH2"]], convert(data6[["CH1"]]), marker='x', s=2, label="all data points")
plt.scatter(data6.iloc[:250,2], convert(data6.iloc[:250,1]), marker='x', s=10, label="first branch")
plt.scatter(data6.iloc[250:500,2], convert(data6.iloc[250:500,1]), marker='x', s=10, label="second branch")
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.grid()
plt.legend()
plt.savefig("10kHz_I-V_hi-res.png")
plt.show()

