# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 14:51:20 2020

@author: wtheo
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import butter, filtfilt
from scipy.optimize import curve_fit

#photodiode characterisation
g=10**1 #gain factor
g_m=3   #gain mutliplier
r=0.65*50*10**-3  #response factor in V/mW

def convert(voltage, gstar=g, g_mstar=g_m):
    return voltage/gstar/g_mstar/r

#%%
#square signal
#1 Hz
#700 mV offset
#30 mW @ 1550nm

set1=pd.read_csv("tek0040.csv", sep=',', header=19)
set2=pd.read_csv("tek0041.csv", sep=',', header=19)
set3=pd.read_csv("tek0042.csv", sep=',', header=19)
set4=pd.read_csv("tek0044.csv", sep=',', header=19)
set5=pd.read_csv("tek0045.csv", sep=',', header=19)
set6=pd.read_csv("tek0046.csv", sep=',', header=19)
set7=pd.read_csv("tek0047.csv", sep=',', header=19)
set8=pd.read_csv("tek0048.csv", sep=',', header=19)

dummy1=np.linspace(0, 2, 5002)

fig=plt.figure(figsize=(12.8, 12.8))
gs=fig.add_gridspec(8,1)
ax1=fig.add_subplot(gs[:6,:])
ax2=fig.add_subplot(gs[6:,:])
ax1.set_title("Slow effect")
ax1.plot(dummy1, convert(set1.iloc[780:5782, 1]), label="1 Volt peak to peak")
ax1.plot(dummy1, convert(set2.iloc[2255:7257, 1]), label="2 Volt peak to peak")
ax1.plot(dummy1, convert(set3.iloc[962:5964, 1]), label="3 Volt peak to peak")
ax1.plot(dummy1, convert(set4.iloc[1811:6813, 1]), label="4 Volt peak to peak")
ax1.plot(dummy1, convert(set5.iloc[1919:6921, 1]), label="5 Volt peak to peak")
ax1.plot(dummy1, convert(set6.iloc[2102:7104, 1]), label="6 Volt peak to peak")
ax1.plot(dummy1, convert(set7.iloc[2034:7036, 1]), label="7 Volt peak to peak")
ax1.plot(dummy1, convert(set8.iloc[665:5667, 1]), label="8 Volt peak to peak")
ax2.plot(dummy1, convert(set1.iloc[780:5782, 2]), label="1 Volt peak to peak")
ax2.plot(dummy1, convert(set2.iloc[2255:7257, 2]), label="2 Volt peak to peak")
ax2.plot(dummy1, convert(set3.iloc[962:5964, 2]), label="3 Volt peak to peak")
ax2.plot(dummy1, convert(set4.iloc[1811:6813, 2]), label="4 Volt peak to peak")
ax2.plot(dummy1, convert(set5.iloc[1919:6921, 2]), label="5 Volt peak to peak")
ax2.plot(dummy1, convert(set6.iloc[2102:7104, 2]), label="6 Volt peak to peak")
ax2.plot(dummy1, convert(set7.iloc[2034:7036, 2]), label="7 Volt peak to peak")
ax2.plot(dummy1, convert(set8.iloc[665:5667, 2]), label="8 Volt peak to peak")
ax1.grid()
ax2.grid()
ax1.legend()
ax2.legend()
ax1.label_outer()
ax2.set_xlabel("Time /s")
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("AC source voltage /V")
plt.savefig("drift_2s.png")
plt.savefig("drift_2s.eps")
plt.show()


dummy5=np.linspace(0, 1, 2501)

fig=plt.figure(figsize=(12.8, 12.8))
gs=fig.add_gridspec(8,1)
ax1=fig.add_subplot(gs[:6,:])
ax2=fig.add_subplot(gs[6:,:])
ax1.set_title("Slow effect")
ax1.plot(dummy5, convert(set1.iloc[780:5782-2501, 1]), label="1 Volt peak to peak")
ax1.plot(dummy5, convert(set2.iloc[2255:7257-2501, 1]), label="2 Volt peak to peak")
ax1.plot(dummy5, convert(set3.iloc[962:5964-2501, 1]), label="3 Volt peak to peak")
ax1.plot(dummy5, convert(set4.iloc[1811:6813-2501, 1]), label="4 Volt peak to peak")
ax1.plot(dummy5, convert(set5.iloc[1919:6921-2501, 1]), label="5 Volt peak to peak")
ax1.plot(dummy5, convert(set6.iloc[2102:7104-2501, 1]), label="6 Volt peak to peak")
ax1.plot(dummy5, convert(set7.iloc[2034:7036-2501, 1]), label="7 Volt peak to peak")
ax1.plot(dummy5, convert(set8.iloc[665:5667-2501, 1]), label="8 Volt peak to peak")
ax2.plot(dummy5, convert(set1.iloc[780:5782-2501, 2]), label="1 Volt peak to peak")
ax2.plot(dummy5, convert(set2.iloc[2255:7257-2501, 2]), label="2 Volt peak to peak")
ax2.plot(dummy5, convert(set3.iloc[962:5964-2501, 2]), label="3 Volt peak to peak")
ax2.plot(dummy5, convert(set4.iloc[1811:6813-2501, 2]), label="4 Volt peak to peak")
ax2.plot(dummy5, convert(set5.iloc[1919:6921-2501, 2]), label="5 Volt peak to peak")
ax2.plot(dummy5, convert(set6.iloc[2102:7104-2501, 2]), label="6 Volt peak to peak")
ax2.plot(dummy5, convert(set7.iloc[2034:7036-2501, 2]), label="7 Volt peak to peak")
ax2.plot(dummy5, convert(set8.iloc[665:5667-2501, 2]), label="8 Volt peak to peak")
ax1.grid()
ax2.grid()
ax1.legend()
ax2.legend()
ax1.label_outer()
ax2.set_xlabel("Time /s")
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("AC source voltage /V")
plt.savefig("drift_1s.png")
plt.savefig("drift_1s.eps")
plt.show()



"""
dummy2=np.linspace(0, 0.5, 1249)
dummy3=np.linspace(0.5, 1, 1249)

b, a=butter(2, 1/100, btype='low', analog=False)

y1=filtfilt(b, a, convert(set1.iloc[781:2030, 1]))
y11=filtfilt(b, a, convert(set1.iloc[2031:3280, 1]))
y2=filtfilt(b, a, convert(set2.iloc[2256:3505, 1]))
y12=filtfilt(b, a, convert(set2.iloc[3506:4755, 1]))
y3=filtfilt(b, a, convert(set3.iloc[963:2212, 1]))
y13=filtfilt(b, a, convert(set3.iloc[2213:3462, 1]))
y4=filtfilt(b, a, convert(set4.iloc[1812:3061, 1]))
y14=filtfilt(b, a, convert(set4.iloc[3062:4311, 1]))
y5=filtfilt(b, a, convert(set5.iloc[1920:3169, 1]))
y15=filtfilt(b, a, convert(set5.iloc[3170:4419, 1]))
y6=filtfilt(b, a, convert(set6.iloc[2103:3352, 1]))
y16=filtfilt(b, a, convert(set6.iloc[3353:4602, 1]))
y7=filtfilt(b, a, convert(set7.iloc[2035:3284, 1]))
y17=filtfilt(b, a, convert(set7.iloc[3285:4534, 1]))
y8=filtfilt(b, a, convert(set8.iloc[666:1915, 1]))
y18=filtfilt(b, a, convert(set8.iloc[1916:3165, 1]))

fig=plt.figure(figsize=(12.8, 9.6))
ax1=fig.add_subplot()
ax1.plot(dummy2, y1, label="1 Volt peak to peak")
ax1.plot(dummy3, y11, color="C0")
ax1.plot([dummy2[-1], dummy3[0]], [y1[-1], y11[0]], color="C0")
ax1.plot(dummy2, y2, label="2 Volt peak to peak")
ax1.plot(dummy3, y12, color="C1")
ax1.plot([dummy2[-1], dummy3[0]], [y2[-1], y12[0]], color="C1")
ax1.plot(dummy2, y3, label="3 Volt peak to peak")
ax1.plot(dummy3, y13, color="C2")
ax1.plot([dummy2[-1], dummy3[0]], [y3[-1], y13[0]], color="C2")
ax1.plot(dummy2, y4, label="4 Volt peak to peak")
ax1.plot(dummy3, y14, color="C3")
ax1.plot([dummy2[-1], dummy3[0]], [y4[-1], y14[0]], color="C3")
ax1.plot(dummy2, y5, label="5 Volt peak to peak")
ax1.plot(dummy3, y15, color="C4")
ax1.plot([dummy2[-1], dummy3[0]], [y5[-1], y15[0]], color="C4")
ax1.plot(dummy2, y6, label="6 Volt peak to peak")
ax1.plot(dummy3, y16, color="C5")
ax1.plot([dummy2[-1], dummy3[0]], [y6[-1], y16[0]], color="C5")
ax1.plot(dummy2, y7, label="7 Volt peak to peak")
ax1.plot(dummy3, y17, color="C6")
ax1.plot([dummy2[-1], dummy3[0]], [y7[-1], y17[0]], color="C6")
ax1.plot(dummy2, y8, label="8 Volt peak to peak")
ax1.plot(dummy3, y18, color="C7")
ax1.plot([dummy2[-1], dummy3[0]], [y8[-1], y18[0]], color="C7")
ax1.grid()
ax1.legend()
ax1.set_ylabel("Optical power /mW")
ax1.set_xlabel("Time /s")
plt.show()
"""


#%%
#tek0034
#10 kHz
#5 V pp.
#880 mV offset
#30 mW @ 1550nm

set9=pd.read_csv("tek0034.csv", sep=',', header=19)
dummy4=np.arange(set9["CH1"].size)/10000*2

fig=plt.figure(figsize=(12.8, 9.6))
gs=fig.add_gridspec(6,1)
ax1=fig.add_subplot(gs[:4,:])
ax2=fig.add_subplot(gs[4:,:])
ax1.set_title("Square signal at 5 kHz with 800 nm modulator")
ax1.plot(dummy4, convert(set9["CH1"], g_mstar=1), label="optical signal")
ax2.plot(dummy4, set9["CH2"], label="electric signal")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage across electrodes /V")
ax2.set_xlabel("Time /ms")
ax1.legend()
ax2.legend()
ax1.grid()
ax2.grid()
plt.savefig("rc-effect.png")
plt.savefig("rc-effect.eps")
plt.show()

def U_C1(time, U_0, tau_C, delx, dely):
    return U_0*(1-np.exp(-(time-delx)/tau_C))+dely

def U_C2(time, U_0, tau_C, delx, dely):
    return U_0*np.exp(-(time-delx)/tau_C)+dely

popt1, pcov1=curve_fit(U_C1, dummy4[:490], convert(set9.iloc[:490, 1], g_mstar=1), p0=[0.045, 0.01, 0, 0.095])
popt2, pcov2=curve_fit(U_C2, dummy4[492:990], convert(set9.iloc[492:990, 1], g_mstar=1), p0=[0.034, 0.006, 0.1, 0.08])

print("f_1(x)=", popt1[0], "*(1-exp(-(t-", popt1[2], ")/", popt1[1], "))+", popt1[3])
print("f_2(x)=", popt2[0], "*exp(-(t-", popt2[2], ")/", popt2[1], ")+", popt2[3])

plt.figure(figsize=(12.8, 9.6))
plt.title("Square signal at 5 kHz with 800 nm modulator")
plt.plot(dummy4[:1000], convert(set9.iloc[:1000, 1], g_mstar=1), label="collected data")
plt.plot(dummy4[:490], U_C1(dummy4[:490], *popt1), linewidth=2, label=r"$f(x)=0.045(1-\exp(-\frac{t}{0.0062}))+0.093$")
plt.plot(dummy4[492:990], U_C2(dummy4[492:990], *popt2), linewidth=2, label=r"$f(x)=0.029\exp(-\frac{t-0.010}{0.0063})+0.082$")
plt.legend()
plt.grid()
plt.xlabel("Time /ms")
plt.ylabel("optical power /mW")
plt.savefig("rc-effect_close-up.png")
plt.savefig("rc-effect_close-up.eps")
plt.show()

#%%



