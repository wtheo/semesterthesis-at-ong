# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 16:04:13 2020

@author: theow
"""

import pandas as pd
from matplotlib import pyplot as plt


points=pd.read_excel("cross-section-modulator.xlsx", header=3)

plt.figure(figsize=(12.8, 9.6))
plt.plot(points["x"]*100, points["y"]*100)
#plt.xlim((0,50))
#plt.ylim((-10,40))
plt.xlabel("x-coordinate /nm")
plt.ylabel("x-coordinate /nm")
plt.grid()
plt.savefig("crosssection.png")
plt.show()


