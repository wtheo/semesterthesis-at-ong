# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 09:43:54 2020

@author: wtheo
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


#photodiode characterisation
g=10**3 #gain factor
g_m=3   #gain mutliplier
r=0.33  #response factor in V/mW

def convert(voltage):
    return voltage/g/g_m/r


#black current, imported from P:/ong-stud/2020/Theo Wollschlegel/01_Data/new_mz-modulator_500nm/10mm_varying_freq/calc.py
avgval=-2.4667474747474757*10**-6


#%%
#calculates power ratio for default input power of 3mW in dB
def pow_spec(value, power=3):
    return 10*np.log((value-avgval)/power)/np.log(10)

#%%
#tek0007
#1 Hz
#20 V pp
#0.0 V offset
#3 mW @770nm

data0=pd.read_csv("tek0007.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000*2

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title(r"1 Hz modulation frequency, 20 V peak-to-peak, $\lambda=770$ nm")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data0[["CH1"]]), color="C0", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data0[["CH2"]], color="C0", linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("1Hz_770nm_V-t.png")
plt.savefig("1Hz_770nm_V-t.eps")
plt.show()

#%%
#tek0009
#1 Hz
#20 V pp
#0.0 V offset
#3 mW @775nm

data1=pd.read_csv("tek0009.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000*2

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title(r"1 Hz modulation frequency, 20 V peak-to-peak, $\lambda=775$ nm")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data1[["CH1"]]), color="C0",marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data1[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("1Hz_775nm_V-t.png")
plt.savefig("1Hz_775nm_V-t.eps")
plt.show()

#%%
#tek0010
#1 Hz
#20 V pp
#0.0 V offset
#3 mW @780nm

data2=pd.read_csv("tek0010.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000*2

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title(r"1 Hz modulation frequency, 20 V peak-to-peak, $\lambda=780$ nm")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data2[["CH1"]]), color="C0",marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data2[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("1Hz_780nm_V-t.png")
plt.savefig("1Hz_780nm_V-t.eps")
plt.show()