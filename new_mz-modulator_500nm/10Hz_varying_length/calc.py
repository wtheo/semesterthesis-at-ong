# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 11:40:39 2020

@author: theow
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.constants import *
from scipy.optimize import *

#photodiode characterisation
g=10**3 #gain factor
g_m=3   #gain mutliplier
r=0.33  #response factor in V/mW

def convert(voltage, gstar=g):
    return voltage/gstar/g_m/r

def findmax(data):
    max=data.iloc[0]
    index=0
    for i in range (data.size):
        if data.iloc[i]>max:
            max=data.iloc[i]
            index=i
    return max, index

def linfunc(x, a, b):
    return a*x+b

def linfit(xdata, ydata, error):
    a0=(ydata[-1]-ydata[0])/(xdata[-1]-xdata[0])
    b0=ydata[-1]-a0*xdata[-1]
    popt, pcov=curve_fit(linfunc, xdata, ydata, p0=[a0, b0], sigma=error, absolute_sigma=True)
    return popt, pcov

def sinfunc(x, a, b, c, d):
    return a*np.sin(2*np.pi/b*x-c)+d

def sinfit(xdata, ydata):
    a0=findmax(ydata)[0]-findmax(-ydata)[0]
    b0=2*(xdata.iloc[findmax(ydata)[1]]-xdata.iloc[findmax(-ydata)[1]])
    c0=xdata.iloc[findmax(ydata)[1]]-b0/4
    d0=np.sum(ydata)/ydata.size
    popt, pcov=curve_fit(sinfunc, xdata, ydata, p0=[a0, b0, c0, d0])
    return popt, pcov

dark=pd.read_csv("tek0002.csv", sep=',', header=19)

#1cm waveguide, 2Vpp, 2.7V offset
len1=pd.read_csv("tek0018.csv", sep=',', header=19)

#0.8cm waveguide, 2Vpp, 3.2V offset
len2=pd.read_csv("tek0021.csv", sep=',', header=19)

#0.6cm waveguide, 3Vpp, 3.5V offset
len3=pd.read_csv("tek0022.csv", sep=',', header=19)

#0.2cm waveguide, 15Vpp, -1.6V offset
len4=pd.read_csv("tek0023.csv", sep=',', header=19)

len4[["CH2"]]/=4

#%%

dummyx=np.linspace(0,2,10000)

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("1 cm waveguide, 10 Hz modulation frequency, 2.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(len1[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, len1[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
plt.savefig("10mmV-t.png")
plt.savefig("10mmV-t.eps")
ax1.legend()
ax2.legend()
plt.show()

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("0.8 cm waveguide, 10 Hz modulation frequency, 2.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx/2, convert(len2[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx/2, len2[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
plt.savefig("8mmV-t.png")
plt.savefig("8mmV-t.eps")
ax1.legend()
ax2.legend()
plt.show()

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("0.6 cm waveguide, 10 Hz modulation frequency, 2.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(len3[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, len3[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
plt.savefig("6mmV-t.png")
plt.savefig("6mmV-t.eps")
ax1.legend()
ax2.legend()
plt.show()

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("0.2 cm waveguide, 10 Hz modulation frequency, 2.0 V amplitude")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(len4[["CH1"]], 10**4), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, len4[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
plt.savefig("2mmV-t.png")
plt.savefig("2mmV-t.eps")
ax1.legend()
ax2.legend()
plt.show()

#%%

popt0, pcov0=sinfit(len1.iloc[185:335,2], convert(len1.iloc[185:335,1]))
print("f_1(x)=", popt0[0], "*sin(", 2*np.pi/popt0[1], "x+", -popt0[2], ")+", popt0[3])
popt00, pcov00=sinfit(len1.iloc[435:585,2], convert(len1.iloc[435:585,1]))
print("f_2(x)=", popt00[0], "*sin(", 2*np.pi/popt00[1], "x+", -popt00[2], ")+", popt00[3])

popt1, pcov1=sinfit(len2.iloc[500:850,2], convert(len2.iloc[500:850,1]))
print("f_1(x)=", popt1[0], "*sin(", 2*np.pi/popt1[1], "x+", -popt1[2], ")+", popt1[3])
popt10, pcov10=sinfit(len2.iloc[1100:1400,2], convert(len2.iloc[1100:1400,1]))
print("f_2(x)=", popt10[0], "*sin(", 2*np.pi/popt10[1], "x+", -popt10[2], ")+", popt10[3])

popt2, pcov2=sinfit(len3.iloc[165:320,2], convert(len3.iloc[165:320,1]))
print("f_1(x)=", popt2[0], "*sin(", 2*np.pi/popt2[1], "x+", -popt2[2], ")+", popt2[3])
popt20, pcov20=sinfit(len3.iloc[415:565,2], convert(len3.iloc[415:565,1]))
print("f_2(x)=", popt20[0], "*sin(", 2*np.pi/popt20[1], "x+", -popt20[2], ")+", popt20[3])

popt3, pcov3=sinfit(len4.iloc[115:285,2], convert(len4.iloc[115:285,1]))
print("f_1(x)=", popt3[0], "*sin(", 2*np.pi/popt3[1], "x+", -popt3[2], ")+", popt3[3])
popt30, pcov30=sinfit(len4.iloc[325:515,2], convert(len4.iloc[325:515,1]))
print("f_2(x)=", popt30[0], "*sin(", 2*np.pi/popt30[1], "x+", -popt30[2], ")+", popt30[3])


plt.figure(figsize=(12.8,9.6))
plt.title("1 cm waveguide, 10 Hz modulation frequency, 2.0 V amplitude")
plt.scatter(len1[["CH2"]], convert(len1[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(len1.iloc[135:385,2], convert(len1.iloc[135:385,1]), marker='x', s=10, label="first branch down")
plt.scatter(len1.iloc[385:635,2], convert(len1.iloc[385:635,1]), marker='x', s=10, label="second branch up")
plt.plot(len1.iloc[135:385,2], sinfunc(len1.iloc[135:385,2], *popt0), color="C4", linewidth=2, label=r"$f(x)=7.9*10^{-6}sin(-3.4x-3.5)+1.6*10^{-5}$")
plt.plot(len1.iloc[385:635,2], sinfunc(len1.iloc[385:635,2], *popt00), color="C5", linewidth=2, label=r"$f(x)=8.0*10^{-6}sin(-3.6x-2.2)+1.6*10^{-5}$")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("10mmV-I.png")
plt.savefig("10mmV-I.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("0.8 cm waveguide, 10 Hz modulation frequency, 2.0 V amplitude")
plt.scatter(len2[["CH2"]], convert(len2[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(len2.iloc[450:950,2], convert(len2.iloc[450:950,1]), marker='x', s=10, label="first branch down")
plt.scatter(len2.iloc[950:1450,2], convert(len2.iloc[950:1450,1]), marker='x', s=10, label="second branch up")
plt.plot(len2.iloc[450:950,2], sinfunc(len2.iloc[450:950,2], *popt1), color="C4", linewidth=2, label=r"$f(x)=3.2*10^{-6}sin(2.8x-2.9)+3.1*10^{-6}$")
plt.plot(len2.iloc[950:1450,2], sinfunc(len2.iloc[950:1450,2], *popt10), color="C5", linewidth=2, label=r"$f(x)=3.3*10^{-6}sin(3.0x-4.2)+3.1*10^{-6}$")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("8mmV-I.png")
plt.savefig("8mmV-I.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("0.6 cm waveguide, 10 Hz modulation frequency, 2.0 V amplitude")
plt.scatter(len3[["CH2"]], convert(len3[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(len3.iloc[115:365,2], convert(len3.iloc[115:365,1]), marker='x', s=10, label="first branch up")
plt.scatter(len3.iloc[365:615,2], convert(len3.iloc[365:615,1]), marker='x', s=10, label="second branch down")
plt.plot(len3.iloc[115:365,2], sinfunc(len3.iloc[115:365,2], *popt2), color="C4", linewidth=2, label=r"$f(x)=-7.9*10^{-6}sin(2.3x-5.4)+2.2*10^{-5}$")
plt.plot(len3.iloc[365:615,2], sinfunc(len3.iloc[365:615,2], *popt20), color="C5", linewidth=2, label=r"$f(x)=-7.8*10^{-6}sin(2.1x-4.1)+2.3*10^{-5}$")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("6mmV-I.png")
plt.savefig("6mmV-I.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("0.2 cm waveguide, 10 Hz modulation frequency, 2.0 V amplitude")
plt.scatter(len4[["CH2"]], convert(len4[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(len4.iloc[45:295,2], convert(len4.iloc[45:295,1]), marker='x', s=10, label="first branch down")
plt.scatter(len4.iloc[295:545,2], convert(len4.iloc[295:545,1]), marker='x', s=10, label="second branch up")
plt.plot(len4.iloc[45:295,2], sinfunc(len4.iloc[45:295,2], *popt3), color="C4", linewidth=2, label=r"$f(x)=-2.0*10^{-5}sin(-1.8x+1.6)+3.6*10^{-5}$")
plt.plot(len4.iloc[295:545,2], sinfunc(len4.iloc[295:545,2], *popt30), color="C5", linewidth=2, label=r"$f(x)=-2.0*10^{-5}sin(-1.5x-0.76)+3.6*10^{-5}$")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("2mmV-I.png")
plt.savefig("2mmV-I.eps")
plt.show()

#%%

points=np.array([[1.0, 0.8, 0.6, 0.2],
                 [np.abs(popt0[1]+popt00[1])/4, np.abs(popt1[1]+popt10[1])/4, np.abs(popt2[1]+popt20[1])/4, np.abs(popt3[1]+popt30[1])/4, ]])

errors=np.array([0.07, 0.1, 0.05, 0.1])

popt, pcov=linfit(points[0,:], points[1,:], errors)
uncertainties=np.sqrt(np.diag(pcov))

print("f(x)=(", popt[0], "+-", uncertainties[0], ")*x+", popt[1], "+-", uncertainties[1])

plt.figure(figsize=(12.8, 9.6))
plt.title(r"$V_{\pi}$ Voltages for modulators with different lengths")
plt.errorbar(points[0,:], points[1,:], yerr=errors, fmt='x', label="Measured samples")
plt.plot(points[0,:], linfunc(points[0,:], *popt), label=r"$f(x)=(-1.5\pm0.14)*x+(2.4\pm0.1)$")
plt.legend()
plt.grid()
plt.xlabel("Length of modulator /cm")
plt.ylabel(r"$V_{\pi}$ Voltage /V")
plt.savefig("v-pi.png")
plt.savefig("v-pi.eps")
plt.show()


