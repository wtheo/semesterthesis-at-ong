# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 09:43:54 2020

@author: wtheo
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


#photodiode characterisation
g=10**3 #gain factor
g_m=3   #gain mutliplier
r=0.33  #response factor in V/mW

def convert(voltage):
    return voltage/g/g_m/r


def findmax(data):
    max=data.iloc[0]
    index=0
    for i in range (data.size):
        if data.iloc[i]>max:
            max=data.iloc[i]
            index=i
    return max, index


def sinfunc(x, a, b, c, d):
    return a*np.sin(2*np.pi/b*x-c)+d


def sinfit(xdata, ydata):
    a0=findmax(ydata)[0]-findmax(-ydata)[0]
    b0=2*(xdata.iloc[findmax(ydata)[1]]-xdata.iloc[findmax(-ydata)[1]])
    c0=xdata.iloc[findmax(ydata)[1]]-b0/4
    d0=np.sum(ydata)/ydata.size
    popt, pcov=curve_fit(sinfunc, xdata, ydata, p0=[a0, b0, c0, d0])
    return popt, pcov


def linfunc(x, a, b):
    return a*x+b


def linfit(xdata, ydata):
    a0=(ydata.iloc[-1]-ydata.iloc[0])/(xdata.iloc[-1]-xdata.iloc[0])
    b0=ydata.iloc[-1]-a0*xdata.iloc[-1]
    popt, pcov=curve_fit(linfunc, xdata, ydata, p0=[a0, b0])
    return popt, pcov

#black current, imported from P:/ong-stud/2020/Theo Wollschlegel/01_Data/new_mz-modulator_500nm/10mm_varying_freq/calc.py
avgval=-2.4667474747474757*10**-6


#%%
#calculates power ratio for default input power of 3mW in dB
def pow_spec(value, power=3):
    return 10*np.log((value-avgval)/power)/np.log(10)

#%%
#tek0001
#100 mHz
#1.7 V pp
#-3500 mV offset
#3 mW @775nm

data0=pd.read_csv("tek0001.csv", sep=',', header=19)
dummyx=np.arange(10000)/1000

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("100 mHz modulation frequency, 1.7 V peak-to-peak")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data0[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data0[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("100mHz_1700mVpp_V-t.png")
plt.show()

popt0, pcov0=sinfit(data0.iloc[50:550,2], convert(data0.iloc[50:550,1]))
print("f_1(x)=", popt0[0], "*sin(", 2*np.pi/popt0[1], "x+", -popt0[2], ")+", popt0[3])

popt00, pcov00=sinfit(data0.iloc[550:1050,2], convert(data0.iloc[550:1050,1]))
print("f_2(x)=", popt00[0], "*sin(", 2*np.pi/popt00[1], "x+", -popt00[2], ")+", popt00[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 100 mHz")
plt.scatter(data0[["CH2"]], convert(data0[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data0.iloc[50:550,2], convert(data0.iloc[50:550,1]), marker='x', s=10, label="first branch down")
plt.scatter(data0.iloc[550:1050,2], convert(data0.iloc[550:1050,1]), marker='x', s=10, label="second branch up")
plt.plot(data0.iloc[50:550,2], sinfunc(data0.iloc[50:550,2], *popt0), color="C4", linewidth=2, label="$f(x)=8.8*10^{-6}sin(-2.5x+3.9)+1.2*10^{-5}$")
plt.plot(data0.iloc[550:1050,2], sinfunc(data0.iloc[550:1050,2], *popt00), color="C5", linewidth=2, label="$f(x)=8.6*10^{-6}sin(-2.4x+4.1)+1.1*10^{-5}$")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("100mHz_1700mVpp_I-V.png")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 100 mHz modulation")
plt.scatter(data0[["CH2"]], pow_spec(convert(data0[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data0.iloc[50:550,2], pow_spec(convert(data0.iloc[50:550,1])), marker='x', s=10, label="first branch down")
plt.scatter(data0.iloc[550:1050,2], pow_spec(convert(data0.iloc[550:1050,1])), marker='x', s=10, label="second branch up")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("100mHz_1700mVpp_I-V_dB.png")
plt.show()

#%%
#tek0002
#1 Hz
#1.8 V pp
#-3.8 mV offset
#3 mW @775nm

data2=pd.read_csv("tek0002.csv", sep=',', header=19)
dummyx=np.arange(10000)/1000*2

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("1 Hz modulation frequency, 1.8 V peak-to-peak")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data2[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data2[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("1Hz_1800mVpp_V-t.png")
plt.show()

popt20, pcov20=sinfit(data2.iloc[60:310,2], convert(data2.iloc[60:310,1]))
print("f_1(x)=", popt20[0], "*sin(", 2*np.pi/popt20[1], "x+", -popt20[2], ")+", popt20[3])

popt21, pcov21=sinfit(data2.iloc[310:560,2], convert(data2.iloc[310:560,1]))
print("f_2(x)=", popt21[0], "*sin(", 2*np.pi/popt21[1], "x+", -popt21[2], ")+", popt21[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 1 Hz")
plt.scatter(data2[["CH2"]], convert(data2[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data2.iloc[60:310,2], convert(data2.iloc[60:310,1]), marker='x', s=10, label="first branch down")
plt.scatter(data2.iloc[310:560,2], convert(data2.iloc[310:560,1]), marker='x', s=10, label="second branch up")
plt.plot(data2.iloc[60:310,2], sinfunc(data2.iloc[60:310,2], *popt20), color="C4", linewidth=2, label="$f(x)=7.4*10^{-6}sin(-2.3x+3.4)+7.7*10^{-6}$")
plt.plot(data2.iloc[310:560,2], sinfunc(data2.iloc[310:560,2], *popt21), color="C5", linewidth=2, label="$f(x)=7.4*10^{-6}sin(-2.3x+3.3)+7.7*10^{-6}$")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("1Hz_1800mVpp_I-V.png")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 1 Hz modulation")
plt.scatter(data2[["CH2"]], pow_spec(convert(data2[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data2.iloc[60:310,2], pow_spec(convert(data2.iloc[60:310,1])), marker='x', s=10, label="first branch down")
plt.scatter(data2.iloc[310:560,2], pow_spec(convert(data2.iloc[310:560,1])), marker='x', s=10, label="second branch up")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("1Hz_1800mVpp_I-V_dB.png")
plt.show()

#%%
#tek0003
#10 Hz
#1.8 V pp
#-3.8 V offset
#3 mW @1550nm

data4=pd.read_csv("tek0003.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000*4

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("10 Hz modulation frequency, 1.8 V peak-to-peak")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data4[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data4[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("10Hz_1800mVpp_V-t.png")
plt.show()

popt40, pcov40=sinfit(data4.iloc[80:205,2], convert(data4.iloc[80:205,1]))
print("f_1(x)=", popt40[0], "*sin(", 2*np.pi/popt40[1], "x+", -popt40[2], ")+", popt40[3])

popt41, pcov41=sinfit(data4.iloc[205:330,2], convert(data4.iloc[205:330,1]))
print("f_2(x)=", popt41[0], "*sin(", 2*np.pi/popt41[1], "x+", -popt41[2], ")+", popt41[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 10 Hz")
plt.scatter(data4[["CH2"]], convert(data4[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data4.iloc[80:205,2], convert(data4.iloc[80:205,1]), marker='x', s=10, label="first branch down")
plt.scatter(data4.iloc[205:330,2], convert(data4.iloc[205:330,1]), marker='x', s=10, label="second branch up")
plt.plot(data4.iloc[80:205,2], sinfunc(data4.iloc[80:205,2], *popt40), color="C4", linewidth=2, label="$f(x)=-7.7*10^{-6}sin(-2.6x+5.5)+7.3*10^{-6}$")
plt.plot(data4.iloc[205:330,2], sinfunc(data4.iloc[205:330,2], *popt41), color="C5", linewidth=2, label="$f(x)=-7.7*10^{-6}sin(-2.6x+5.4)+7.0*10^{-6}$")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("10Hz_1800mVpp_I-V.png")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 10 Hz modulation")
plt.scatter(data4[["CH2"]], pow_spec(convert(data4[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data4.iloc[80:205,2], pow_spec(convert(data4.iloc[80:205,1])), marker='x', s=10, label="first branch down")
plt.scatter(data4.iloc[205:330,2], pow_spec(convert(data4.iloc[205:330,1])), marker='x', s=10, label="second branch up")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("10Hz_1800mVpp_I-V_dB.png")
plt.show()

#%%
#tek0004
#100 Hz
#1.8 V pp
#-4.0 V offset
#3 mW @775nm

data6=pd.read_csv("tek0004.csv", sep=',', header=19)
dummyx=np.arange(10000)/100000*4

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("100 Hz modulation frequency, 1.8 V peak-to-peak")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data6[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data6[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("100Hz_1800mVpp_V-t.png")
plt.show()

popt60, pcov60=sinfit(data6.iloc[25:150,2], convert(data6.iloc[25:150,1]))
print("f_1(x)=", popt60[0], "*sin(", 2*np.pi/popt60[1], "x+", -popt60[2], ")+", popt60[3])

popt61, pcov61=sinfit(data6.iloc[150:275,2], convert(data6.iloc[150:275,1]))
print("f_2(x)=", popt61[0], "*sin(", 2*np.pi/popt61[1], "x+", -popt61[2], ")+", popt61[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 100 Hz")
plt.scatter(data6[["CH2"]], convert(data6[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data6.iloc[25:150,2], convert(data6.iloc[25:150,1]), marker='x', s=10, label="first branch up")
plt.scatter(data6.iloc[150:275,2], convert(data6.iloc[150:275,1]), marker='x', s=10, label="second branch down")
plt.plot(data6.iloc[25:150,2], sinfunc(data6.iloc[25:150,2], *popt60), color="C4", linewidth=2, label="$f(x)=-7.8*10^{-6}sin(-2.5x+5.7)+7.5*10^{-6}$")
plt.plot(data6.iloc[150:275,2], sinfunc(data6.iloc[150:275,2], *popt61), color="C5", linewidth=2, label="$f(x)=-7.7*10^{-6}sin(-2.6x+4.9)+6.9*10^{-6}$")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("100Hz_1800mVpp_I-V.png")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 100 Hz modulation")
plt.scatter(data6[["CH2"]], pow_spec(convert(data6[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data6.iloc[25:150,2], pow_spec(convert(data6.iloc[25:150,1])), marker='x', s=10, label="first branch up")
plt.scatter(data6.iloc[150:275,2], pow_spec(convert(data6.iloc[150:275,1])), marker='x', s=10, label="second branch down")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("100Hz_1800mVpp_I-V_dB.png")
plt.show()

#%%
#tek0005
#10 kHz
#2.7 V pp
#-4.2 V offset
#3 mW @775nm

data10=pd.read_csv("tek0005.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000000*4

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("10 kHz modulation frequency, 2.7 V peak-to-peak")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data10[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data10[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.plot(dummyx[35:160], convert(data10.iloc[35:160,1]), color="orange", label="first branch up")
ax1.plot(dummyx[160:285], convert(data10.iloc[160:285,1]), color="green", label="second branch down")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
ax1.set_xlim((0,0.0008))
ax2.set_xlim((0,0.0008))
ax1.legend()
ax2.legend()
plt.savefig("10kHz_2700mVpp_V-t.png")
plt.show()

popt100, pcov100=sinfit(data10.iloc[35:160,2], convert(data10.iloc[35:160,1]))
print("f_1(x)=", popt100[0], "*sin(", 2*np.pi/popt100[1], "x+", -popt100[2], ")+", popt100[3])

popt101, pcov101=sinfit(data10.iloc[160:285,2], convert(data10.iloc[160:285,1]))
print("f_2(x)=", popt101[0], "*sin(", 2*np.pi/popt101[1], "x+", -popt101[2], ")+", popt101[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 10 kHz")
plt.scatter(data10[["CH2"]], convert(data10[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data10.iloc[35:160,2], convert(data10.iloc[35:160,1]), marker='x', s=10, label="first branch up")
plt.scatter(data10.iloc[160:285,2], convert(data10.iloc[160:285,1]), marker='x', s=10, label="second branch down")
plt.plot(data10.iloc[35:160,2], sinfunc(data10.iloc[35:160,2], *popt100), color="C4", linewidth=2, label="f(x)=-6.8*10^{-6}sin(-1.4x+3.4)+6.7*10^{-6}")
plt.plot(data10.iloc[160:285,2], sinfunc(data10.iloc[160:285,2], *popt101), color="C5", linewidth=2, label="f(x)=7.4*10^{-6}sin(-1.4x+6.0)+6.4*10^{-6}")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("10kHz_2700mVpp_I-V.png")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 10 kHz modulation")
plt.scatter(data10[["CH2"]], pow_spec(convert(data10[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data10.iloc[35:160,2], pow_spec(convert(data10.iloc[35:160,1])), marker='x', s=10, label="first branch up")
plt.scatter(data10.iloc[160:285,2], pow_spec(convert(data10.iloc[160:285,1])), marker='x', s=10, label="second branch down")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("10kHz_2700mVpp_I-V_dB.png")
plt.show()

#%%
#tek0006
#1 Hz
#20 V pp
#0.0 V offset
#3 mW @775nm


data11=pd.read_csv("tek0006.csv", sep=',', header=19)
dummyx=np.arange(10000)/1000

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("1 Hz modulation frequency, 20 V peak-to-peak")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data11[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data11[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.plot(dummyx[340:840], convert(data11.iloc[340:840,1]), color="orange", label="first branch up")
ax2.plot(dummyx[340:840], data11.iloc[340:840,2], color="orange", label="first branch up")
ax1.plot(dummyx[840:1340], convert(data11.iloc[840:1340,1]), color="green", label="second branch down")
ax2.plot(dummyx[840:1340], data11.iloc[840:1340,2], color="green", label="second branch down")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
ax1.legend()
ax2.legend()
plt.savefig("1Hz_20Vpp_V-t.png")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 1 Hz at high amplitude")
plt.scatter(data11.iloc[340:840,2], convert(data11.iloc[340:840,1]), color="orange", marker='x', s=10, label="first branch up")
plt.scatter(data11.iloc[840:1340,2], convert(data11.iloc[840:1340,1]), color="green", marker='x', s=10, label="second branch down")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("10kHz_2700mVpp_I-V.png")
plt.show()
