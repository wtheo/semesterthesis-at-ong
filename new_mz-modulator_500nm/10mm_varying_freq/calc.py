# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 09:43:54 2020

@author: wtheo
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


#photodiode characterisation
g=10**3 #gain factor
g_m=3   #gain mutliplier
r=0.33  #response factor in V/mW

def convert(voltage):
    return voltage/g/g_m/r


def findmax(data):
    max=data.iloc[0]
    index=0
    for i in range (data.size):
        if data.iloc[i]>max:
            max=data.iloc[i]
            index=i
    return max, index


def sinfunc(x, a, b, c, d):
    return a*np.sin(2*np.pi/b*x-c)+d


def sinfit(xdata, ydata):
    a0=findmax(ydata)[0]-findmax(-ydata)[0]
    b0=2*(xdata.iloc[findmax(ydata)[1]]-xdata.iloc[findmax(-ydata)[1]])
    c0=xdata.iloc[findmax(ydata)[1]]-b0/4
    d0=np.sum(ydata)/ydata.size
    popt, pcov=curve_fit(sinfunc, xdata, ydata, p0=[a0, b0, c0, d0])
    return popt, pcov


def linfunc(x, a, b):
    return a*x+b


def linfit(xdata, ydata):
    a0=(ydata.iloc[-1]-ydata.iloc[0])/(xdata.iloc[-1]-xdata.iloc[0])
    b0=ydata.iloc[-1]-a0*xdata.iloc[-1]
    popt, pcov=curve_fit(linfunc, xdata, ydata, p0=[a0, b0])
    return popt, pcov

#%%
#dark current
#tek0001
#no electric signal
#no optical signal

data6=pd.read_csv("tek0001.csv", sep=',', header=19)
d={"col1": np.arange(10000)/10000*0.4}
dummyx=pd.DataFrame(data=d)

avgval=sum(data6["CH1"])/data6.shape[0]
avg = pd.DataFrame(np.zeros(10000), columns=["col1"])
avg["col1"] = avgval

plt.figure(figsize=(12.8,9.6))
plt.title("Dark current")
plt.plot(dummyx[["col1"]], data6[["CH1"]], linewidth=1)
plt.plot(dummyx[["col1"]], avg[["col1"]], label="average")
plt.ylabel("Signal Voltage /V")
plt.xlabel("Time /s")
#plt.ylim((-1*10**-6, -3.5*10**-6))
plt.grid()
plt.legend()
plt.savefig("dark_current.png")
plt.show()

print("avgval=", convert(avgval))

#%%
#calculates power ratio for default input power of 3mW in dB
def pow_spec(value, power=3):
    return 10*np.log((value-convert(avgval))/power)/np.log(10)

#%%
#tek0011
#100 mHz
#2.5 V pp
#2800 mV offset
#3 mW @775nm

data0=pd.read_csv("tek0011.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000*40

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("100 mHz modulation frequency, 2.5 V peak-to-peak")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data0[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data0[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("100mHz_2500mVpp_V-t.png")
plt.savefig("100mHz_2500mVpp_V-t.eps")
plt.show()

popt0, pcov0=sinfit(data0.iloc[850:1700,2], convert(data0.iloc[850:1700,1]))
print("f_1(x)=", popt0[0], "*sin(", 2*np.pi/popt0[1], "x+", -popt0[2], ")+", popt0[3])

popt00, pcov00=sinfit(data0.iloc[2100:2950,2], convert(data0.iloc[2100:2950,1]))
print("f_2(x)=", popt00[0], "*sin(", 2*np.pi/popt00[1], "x+", -popt00[2], ")+", popt00[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 100 mHz")
plt.scatter(data0[["CH2"]], convert(data0[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data0.iloc[650:1900,2], convert(data0.iloc[650:1900,1]), marker='x', s=10, label="first branch up")
plt.scatter(data0.iloc[1900:3150,2], convert(data0.iloc[1900:3150,1]), marker='x', s=10, label="second branch down")
plt.plot(data0.iloc[650:1900,2], sinfunc(data0.iloc[650:1900,2], *popt0), color="C4", linewidth=2, label="$f(x)=9.2*10^{-6}sin(2.8x-1.7)+2.5*10^{-5}$")
plt.plot(data0.iloc[1900:3150,2], sinfunc(data0.iloc[1900:3150,2], *popt00), color="C5", linewidth=2, label="$f(x)=8.7*10^{-6}sin(2.9x-2.0)+2.5*10^{-5}$")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("100mHz_2500mVpp_I-V.png")
plt.savefig("100mHz_2500mVpp_I-V.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 100 mHz modulation")
plt.scatter(data0[["CH2"]], pow_spec(convert(data0[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data0.iloc[650:1900,2], pow_spec(convert(data0.iloc[650:1900,1])), marker='x', s=10, label="first branch up")
plt.scatter(data0.iloc[1900:3150,2], pow_spec(convert(data0.iloc[1900:3150,1])), marker='x', s=10, label="second branch down")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("100mHz_2500mVpp_I-V_dB.png")
plt.savefig("100mHz_2500mVpp_I-V_dB.eps")
plt.show()

ex_ratio0a=findmax(pow_spec(convert(data0.iloc[650:1900,1])))[0]+findmax(-pow_spec(convert(data0.iloc[650:1900,1])))[0]
ex_ratio0b=findmax(pow_spec(convert(data0.iloc[1900:3150,1])))[0]+findmax(-pow_spec(convert(data0.iloc[1900:3150,1])))[0]
ex_ratio0=np.array([(ex_ratio0a+ex_ratio0b)/2, np.abs(ex_ratio0a-ex_ratio0b)/2])

#%%
#tek0012
#1 Hz
#5 V pp
#2800 mV offset
#3 mW @775nm

data2=pd.read_csv("tek0012.csv", sep=',', header=19)
dummyx=np.arange(10000)/1000

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("1 Hz modulation frequency, 2.5 V peak-to-peak")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data2[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data2[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("1Hz_2500mVpp_V-t.png")
plt.savefig("1Hz_2500mVpp_V-t.eps")
plt.show()

popt20, pcov20=sinfit(data2.iloc[300:600,2], convert(data2.iloc[300:600,1]))
print("f_1(x)=", popt20[0], "*sin(", 2*np.pi/popt20[1], "x+", -popt20[2], ")+", popt20[3])

popt21, pcov21=sinfit(data2.iloc[810:1110,2], convert(data2.iloc[810:1110,1]))
print("f_2(x)=", popt21[0], "*sin(", 2*np.pi/popt21[1], "x+", -popt21[2], ")+", popt21[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 1 Hz")
plt.scatter(data2[["CH2"]], convert(data2[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data2.iloc[210:710,2], convert(data2.iloc[210:710,1]), marker='x', s=10, label="first branch down")
plt.scatter(data2.iloc[710:1210,2], convert(data2.iloc[710:1210,1]), marker='x', s=10, label="second branch up")
plt.plot(data2.iloc[210:710,2], sinfunc(data2.iloc[210:710,2], *popt20), color="C4", linewidth=2, label="$f(x)=8.76*10^{-6}sin(2.8x-1.9)+2.5*10^{-5}$")
plt.plot(data2.iloc[710:1210,2], sinfunc(data2.iloc[710:1210,2], *popt21), color="C5", linewidth=2, label="$f(x)=8.2*10^{-6}sin(2.8x-1.6)+2.4*10^{-5}$")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("1Hz_2500mVpp_I-V.png")
plt.savefig("1Hz_2500mVpp_I-V.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 1 Hz modulation")
plt.scatter(data2[["CH2"]], pow_spec(convert(data2[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data2.iloc[210:710,2], pow_spec(convert(data2.iloc[210:710,1])), marker='x', s=10, label="first branch down")
plt.scatter(data2.iloc[710:1210,2], pow_spec(convert(data2.iloc[710:1210,1])), marker='x', s=10, label="second branch up")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("1Hz_2500mVpp_I-V_dB.png")
plt.savefig("1Hz_2500mVpp_I-V_dB.eps")
plt.show()

ex_ratio2a=findmax(pow_spec(convert(data2.iloc[210:710,1])))[0]+findmax(-pow_spec(convert(data2.iloc[210:710,1])))[0]
ex_ratio2b=findmax(pow_spec(convert(data2.iloc[710:1210,1])))[0]+findmax(-pow_spec(convert(data2.iloc[710:1210,1])))[0]
ex_ratio2=np.array([(ex_ratio2a+ex_ratio2b)/2, np.abs(ex_ratio2a-ex_ratio2b)/2])

#%%
#tek0013
#10 Hz
#2.5 V pp
#2800 mV offset
#3 mW @1550nm

data4=pd.read_csv("tek0013.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("10 Hz modulation frequency, 2.5 V peak-to-peak")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data4[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data4[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("10Hz_2500mVpp_V-t.png")
plt.savefig("10Hz_2500mVpp_V-t.eps")
plt.show()

popt40, pcov40=sinfit(data4.iloc[500:800,2], convert(data4.iloc[500:800,1]))
print("f_1(x)=", popt40[0], "*sin(", 2*np.pi/popt40[1], "x+", -popt40[2], ")+", popt40[3])

popt41, pcov41=sinfit(data4.iloc[1000:1300,2], convert(data4.iloc[1000:1300,1]))
print("f_2(x)=", popt41[0], "*sin(", 2*np.pi/popt41[1], "x+", -popt41[2], ")+", popt41[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 10 Hz")
plt.scatter(data4[["CH2"]], convert(data4[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data4.iloc[400:900,2], convert(data4.iloc[400:900,1]), marker='x', s=10, label="first branch up")
plt.scatter(data4.iloc[900:1400,2], convert(data4.iloc[900:1400,1]), marker='x', s=10, label="second branch down")
plt.plot(data4.iloc[400:900,2], sinfunc(data4.iloc[400:900,2], *popt40), color="C4", linewidth=2, label="$f(x)=5.9*10^{-6}sin(3.4x-3.1)+2.4*10^{-5}$")
plt.plot(data4.iloc[900:1400,2], sinfunc(data4.iloc[900:1400,2], *popt41), color="C5", linewidth=2, label="$f(x)=5.8*10^{-6}sin(3.4x-3.7)+2.3*10^{-5}$")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("10Hz_2500mVpp_I-V.png")
plt.savefig("10Hz_2500mVpp_I-V.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 10 Hz modulation")
plt.scatter(data4[["CH2"]], pow_spec(convert(data4[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data4.iloc[400:900,2], pow_spec(convert(data4.iloc[400:900,1])), marker='x', s=10, label="first branch up")
plt.scatter(data4.iloc[900:1400,2], pow_spec(convert(data4.iloc[900:1400,1])), marker='x', s=10, label="second branch down")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("10Hz_2500mVpp_I-V_dB.png")
plt.savefig("10Hz_2500mVpp_I-V_dB.eps")
plt.show()

ex_ratio4a=findmax(pow_spec(convert(data4.iloc[500:800,1])))[0]+findmax(-pow_spec(convert(data4.iloc[500:800,1])))[0]
ex_ratio4b=findmax(pow_spec(convert(data4.iloc[1000:1300,1])))[0]+findmax(-pow_spec(convert(data4.iloc[1000:1300,1])))[0]
ex_ratio4=np.array([(ex_ratio4a+ex_ratio4b)/2, np.abs(ex_ratio4a-ex_ratio4b)/2])

#%%
#tek0015
#100 Hz
#2.5 V pp
#2800 mV offset
#3 mW @775nm

data6=pd.read_csv("tek0015.csv", sep=',', header=19)
dummyx=np.arange(10000)/100000

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("100 Hz modulation frequency, 2.5 V peak-to-peak")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data6[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data6[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("100Hz_2500mVpp_V-t.png")
plt.savefig("100Hz_2500mVpp_V-t.eps")
plt.show()

popt60, pcov60=sinfit(data6.iloc[530:830,2], convert(data6.iloc[530:830,1]))
print("f_1(x)=", popt60[0], "*sin(", 2*np.pi/popt60[1], "x+", -popt60[2], ")+", popt60[3])

popt61, pcov61=sinfit(data6.iloc[1030:1330,2], convert(data6.iloc[1030:1330,1]))
print("f_2(x)=", popt61[0], "*sin(", 2*np.pi/popt61[1], "x+", -popt61[2], ")+", popt61[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 100 Hz")
plt.scatter(data6[["CH2"]], convert(data6[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data6.iloc[430:930,2], convert(data6.iloc[430:930,1]), marker='x', s=10, label="first branch up")
plt.scatter(data6.iloc[930:1430,2], convert(data6.iloc[930:1430,1]), marker='x', s=10, label="second branch down")
plt.plot(data6.iloc[430:930,2], sinfunc(data6.iloc[430:930,2], *popt60), color="C4", linewidth=2, label="$f(x)=8.4*10^{-6}sin(3.6x-4.2)+2.7*10^{-5}$")
plt.plot(data6.iloc[930:1430,2], sinfunc(data6.iloc[930:1430,2], *popt61), color="C5", linewidth=2, label="$f(x)=8.3*10^{-6}sin(3.7x-3.9)+2.7*10^{-5}$")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("100Hz_2500mVpp_I-V.png")
plt.savefig("100Hz_2500mVpp_I-V.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 100 Hz modulation")
plt.scatter(data6[["CH2"]], pow_spec(convert(data6[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data6.iloc[430:930,2], pow_spec(convert(data6.iloc[430:930,1])), marker='x', s=10, label="first branch up")
plt.scatter(data6.iloc[930:1430,2], pow_spec(convert(data6.iloc[930:1430,1])), marker='x', s=10, label="second branch down")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("100Hz_2500mVpp_I-V_dB.png")
plt.savefig("100Hz_2500mVpp_I-V_dB.eps")
plt.show()

ex_ratio6a=findmax(pow_spec(convert(data6.iloc[480:830,1])))[0]+findmax(-pow_spec(convert(data6.iloc[480:830,1])))[0]
ex_ratio6b=findmax(pow_spec(convert(data6.iloc[1030:1400,1])))[0]+findmax(-pow_spec(convert(data6.iloc[1030:1400,1])))[0]
ex_ratio6=np.array([(ex_ratio6a+ex_ratio6b)/2, np.abs(ex_ratio6a-ex_ratio6b)/2])

#%%
#tek0016
#1 kHz
#2.5 V pp
#2500 mV offset
#3 mW @775nm

data8=pd.read_csv("tek0016.csv", sep=',', header=19)
dummyx=np.arange(10000)/1000000

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("1 kHz modulation frequency, 2.5 V peak-to-peak")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data8[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data8[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((10,10.5))
#ax2.set_xlim((10,10.5))
ax1.legend()
ax2.legend()
plt.savefig("1kHz_2500mVpp_V-t.png")
plt.savefig("1kHz_2500mVpp_V-t.eps")
plt.show()

popt80, pcov80=sinfit(data8.iloc[180:480,2], convert(data8.iloc[180:480,1]))
print("f_1(x)=", popt80[0], "*sin(", 2*np.pi/popt80[1], "x+", -popt80[2], ")+", popt80[3])

popt81, pcov81=sinfit(data8.iloc[680:980,2], convert(data8.iloc[680:980,1]))
print("f_2(x)=", popt81[0], "*sin(", 2*np.pi/popt81[1], "x+", -popt81[2], ")+", popt81[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 1 kHz")
plt.scatter(data8[["CH2"]], convert(data8[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data8.iloc[80:580,2], convert(data8.iloc[80:580,1]), marker='x', s=10, label="first branch up")
plt.scatter(data8.iloc[580:1080,2], convert(data8.iloc[580:1080,1]), marker='x', s=10, label="second branch down")
plt.plot(data8.iloc[80:580,2], sinfunc(data8.iloc[80:580,2], *popt80), color="C4", linewidth=2, label="$f(x)=7.5*10^{-6}sin(2.5x-0.95)+2.8*10^{-5}$")
plt.plot(data8.iloc[580:1080,2], sinfunc(data8.iloc[580:1080,2], *popt81), color="C5", linewidth=2, label="$f(x)=-7.2*10^{-6}sin(2.6x-3.4)+2.8*10^{-5}$")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("1kHz_2500mVpp_I-V.png")
plt.savefig("1kHz_2500mVpp_I-V.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 1 kHz modulation")
plt.scatter(data8[["CH2"]], pow_spec(convert(data8[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data8.iloc[80:580,2], pow_spec(convert(data8.iloc[80:580,1])), marker='x', s=10, label="first branch up")
plt.scatter(data8.iloc[580:1080,2], pow_spec(convert(data8.iloc[580:1080,1])), marker='x', s=10, label="second branch down")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("1kHz_2500mVpp_I-V_dB.png")
plt.savefig("1kHz_2500mVpp_I-V_dB.eps")
plt.show()

ex_ratio8a=findmax(pow_spec(convert(data8.iloc[80:580,1])))[0]+findmax(-pow_spec(convert(data8.iloc[80:580,1])))[0]
ex_ratio8b=findmax(pow_spec(convert(data8.iloc[580:1080,1])))[0]+findmax(-pow_spec(convert(data8.iloc[580:1080,1])))[0]
ex_ratio8=np.array([(ex_ratio8a+ex_ratio8b)/2, np.abs(ex_ratio8a-ex_ratio8b)/2])

#%%
#tek0017
#10 kHz
#2.5 V pp
#2500 mV offset
#3 mW @775nm

data10=pd.read_csv("tek0017.csv", sep=',', header=19)
dummyx=np.arange(10000)/10000000

fig=plt.figure(figsize=(12.8,9.6))
gs=fig.add_gridspec(6,3)
ax1=fig.add_subplot(gs[:4,:])
ax1.set_title("10 kHz modulation frequency, 2.5 V peak-to-peak")
ax2=fig.add_subplot(gs[4:,:])
ax1.plot(dummyx, convert(data10[["CH1"]]), color="C0",linestyle="None", marker='.', markersize=2, label="Optical signal")
ax2.plot(dummyx, data10[["CH2"]], color="C0",linestyle="None", marker='.', markersize=2, label="Voltage across electrodes")
ax1.plot(dummyx[250:750], convert(data10.iloc[250:750,1]), color="orange", label="first branch down")
ax1.plot(dummyx[750:1250], convert(data10.iloc[750:1250,1]), color="green", label="second branch up")
ax1.label_outer()
ax1.set_ylabel("Optical power /mW")
ax2.set_ylabel("Voltage /V")
ax2.set_xlabel("Time /s")
ax1.grid()
ax2.grid()
#ax1.set_xlim((0,0.00025))
#ax2.set_xlim((0,0.00025))
ax1.legend()
ax2.legend()
plt.savefig("10kHz_2500mVpp_V-t.png")
plt.savefig("10kHz_2500mVpp_V-t.eps")
plt.show()

popt100, pcov100=sinfit(data10.iloc[350:650,2], convert(data10.iloc[350:650,1]))
print("f_1(x)=", popt100[0], "*sin(", 2*np.pi/popt100[1], "x+", -popt100[2], ")+", popt100[3])

popt101, pcov101=sinfit(data10.iloc[850:1150,2], convert(data10.iloc[850:1150,1]))
print("f_2(x)=", popt101[0], "*sin(", 2*np.pi/popt101[1], "x+", -popt101[2], ")+", popt101[3])

plt.figure(figsize=(12.8,9.6))
plt.title("Intensity optical output vs voltage applied across electrodes for 10 kHz")
plt.scatter(data10[["CH2"]], convert(data10[["CH1"]]), marker='x', s=2, label="All data points")
plt.scatter(data10.iloc[250:750,2], convert(data10.iloc[250:750,1]), marker='x', s=10, label="first branch down")
plt.scatter(data10.iloc[750:1250,2], convert(data10.iloc[750:1250,1]), marker='x', s=10, label="second branch up")
plt.plot(data10.iloc[250:750,2], sinfunc(data10.iloc[250:750,2], *popt100), color="C4", linewidth=2, label="$f(x)=-5.5*10^{-6}sin(2.1x-1.4)+2.7*10^{-5}$")
plt.plot(data10.iloc[750:1250,2], sinfunc(data10.iloc[750:1250,2], *popt101), color="C5", linewidth=2, label="$f(x)=-5.4*10^{-6}sin(2.2x-3.7)+2.6*10^{-5}$")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Optical power /mW")
plt.savefig("10kHz_2500mVpp_I-V.png")
plt.savefig("10kHz_2500mVpp_I-V.eps")
plt.show()

plt.figure(figsize=(12.8,9.6))
plt.title("Extinction of optical power for 10 kHz modulation")
plt.scatter(data10[["CH2"]], pow_spec(convert(data10[["CH1"]])), marker='x', s=2, label="All data points")
plt.scatter(data10.iloc[250:750,2], pow_spec(convert(data10.iloc[250:750,1])), marker='x', s=10, label="first branch down")
plt.scatter(data10.iloc[750:1250,2], pow_spec(convert(data10.iloc[750:1250,1])), marker='x', s=10, label="second branch up")
plt.legend()
plt.grid()
plt.xlabel("Voltage /V")
plt.ylabel("Extinction ratio (dB)")
plt.savefig("10kHz_2500mVpp_I-V_dB.png")
plt.savefig("10kHz_2500mVpp_I-V_dB.eps")
plt.show()

ex_ratio10a=findmax(pow_spec(convert(data10.iloc[250:750,1])))[0]+findmax(-pow_spec(convert(data10.iloc[250:750,1])))[0]
ex_ratio10b=findmax(pow_spec(convert(data10.iloc[750:1250,1])))[0]+findmax(-pow_spec(convert(data10.iloc[750:1250,1])))[0]
ex_ratio10=np.array([(ex_ratio10a+ex_ratio10b)/2, np.abs(ex_ratio10a-ex_ratio10b)/2])

#%%

V_pi=np.array([(popt00[1]+popt0[1])/4, (popt20[1]+popt21[1])/4, (popt41[1]+popt40[1])/4, (popt60[1]+popt61[1])/4, (popt80[1]+popt81[1])/4, (popt100[1]+popt101[1])/4])

freq=np.array([0.1, 1, 10, 100, 1000, 10000])

plt.figure(figsize=(12.8, 9.6))
plt.scatter(np.log(freq)/np.log(10), V_pi, marker='x', label="measurements")
plt.legend()
plt.grid()
plt.xlabel("Frequency dB/Hz")
plt.ylabel("$V_\pi$ Voltage /V")
plt.show()


ex_ratios=np.stack((ex_ratio0, ex_ratio2, ex_ratio4, ex_ratio6, ex_ratio8, ex_ratio10))

print(ex_ratios)

